package PuffySaga.handlers;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

import PuffySaga.PuffySagaExtension;
import PuffySaga.simulation.CombatPlayer;
import PuffySaga.simulation.Transform;
import PuffySaga.simulation.World;
import PuffySaga.utils.RoomHelper;

public class SpawnMeHandler extends BaseClientRequestHandler
{
	@Override
	public void handleClientRequest(User u, ISFSObject data)
	{	World world = RoomHelper.getWorld(this);

		Transform ntrans = Transform.fromSFSObject(data);

		boolean newPlayer = world.addOrRespawnPlayer(u, ntrans);

		if (newPlayer)
		{	// Send this player data about all the other players
			sendOtherPlayersInfo(u);
		}

		// Instantiating more items together with spawning player
		world.spawnItems();
	}

	// Send the data for all the other players to the newly joined client
	private void sendOtherPlayersInfo(User targetUser)
	{	World world = RoomHelper.getWorld(this);
		for (CombatPlayer player : world.getPlayers())
		{	if (player.isDead())
			{	continue;
			}

			if (player.getSfsUser().getId() != targetUser.getId())
			{	PuffySagaExtension ext = (PuffySagaExtension) this.getParentExtension();
				ext.clientInstantiatePlayer(player, targetUser);
			}
		}
	}
}
