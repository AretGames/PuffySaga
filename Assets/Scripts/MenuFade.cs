﻿using UnityEngine;
using System.Collections;

public class MenuFade
{
	public RenderTexture	m_rt;
	public Texture2D		m_texa;
	public Texture2D		m_texb;
	public bool				m_sens;		//true=in, false=out
	public bool				m_isFading = false;
	public bool				m_isFadeStarted;
	public bool				m_isFadeActive=false;
	public float			m_fadeFactor;
	public int				m_isGrabNewScene=0;
	public int				m_hasGrabNewScene=0;
	public Rect				m_recta = new Rect();
	public Rect				m_rectb = new Rect();
	public GameObject		m_all=null;
	public GameObject		m_allcam=null;
	public GameObject		m_bgcam=null;
	public bool				m_isrtWarm;
	public bool				m_isrt;

	public void Init(bool sens)
	{
		m_rt = new RenderTexture(Screen.width, Screen.height,24,RenderTextureFormat.ARGB32);
		m_texa = new Texture2D(Screen.width,Screen.height,TextureFormat.ARGB32, false);
		m_texb = new Texture2D(Screen.width,Screen.height,TextureFormat.ARGB32, false);

		m_sens = sens;
		m_recta.position = new Vector2(0,0);
		m_recta.size = new Vector2(Screen.width, Screen.height);
		m_rectb.position = new Vector2(0,0);
		m_rectb.size = new Vector2(Screen.width, Screen.height);
		m_isFading = false;
		m_isFadeStarted = false;
		m_isFadeActive = true;
		m_fadeFactor = 0.0f;
		m_isGrabNewScene=0;
		m_hasGrabNewScene=0;
		m_all = null;
		m_isrt = false;
		m_isrtWarm = false;
		m_bgcam = new GameObject("MenuFadeBGCamera");
		Camera cam = m_bgcam.AddComponent<Camera>();
		cam.backgroundColor = Color.black;
		cam.clearFlags = CameraClearFlags.Color;
		m_bgcam.SetActive(false);
	}

	public void SetMenuFadeRenderTarget()
	{	SetMenuFadeRenderTarget(m_isrt);
	}

	public void SetMenuFadeRenderTarget(bool isSet)
	{	m_isrt = isSet;
		if (isSet)
		{	if (m_rt == null)
			{	m_rt = new RenderTexture(Screen.width, Screen.height,24,RenderTextureFormat.ARGB32);
			}
		}
		else
		{	m_rt = null;
		}
		RenderTexture.active = m_rt;
	}

	public void SceneLoaded(int level)
	{
		//if (m_isFadeActive == false) return;
		m_isFading = true;
		m_isGrabNewScene = 1;
		m_isFadeStarted = false;

		SetMenuFadeRenderTarget(true);
	}

	public IEnumerator RenderSceneToTexture(Texture2D tex)
	{	yield return new WaitForEndOfFrame();
		tex.ReadPixels(m_recta, 0, 0);
		tex.Apply ();
		SetMenuFadeRenderTarget();
	}
	void SetMenuVisible(bool state)
	{	if (m_all == null)
		{	m_all = GameObject.Find ("/All");
			m_allcam = GameObject.Find ("/Main Camera");
			m_bgcam = new GameObject();
			Camera cam = m_bgcam.AddComponent<Camera>();
			cam.backgroundColor = Color.black;
			cam.clearFlags = CameraClearFlags.Color;
		}
		if (m_all)
		{	m_all.SetActive(state);
			m_allcam.SetActive(state);
			m_bgcam.SetActive(!state);
		}
	}
	public void Update(float elapsedtime)
	{	if (m_isFading==false) return;
		if (m_isFadeActive==false) return;

		SetMenuFadeRenderTarget();

		if (m_isGrabNewScene>0)
		{	if (--m_isGrabNewScene<=0)
			{	m_isGrabNewScene = 0;
				m_hasGrabNewScene = 1;
				Messenger.Broadcast(EVENT.MENUFADE_GET_TEXB);
			}
			return;
		}
		if (m_hasGrabNewScene>0)
		{	if (--m_hasGrabNewScene<=0)
			{	m_hasGrabNewScene = 0;
				SetMenuFadeRenderTarget(false);
				SetMenuVisible(false);
				m_isrtWarm = true;
				m_isFadeStarted = true;
			}
			return;
		}
		
		if (m_isFadeStarted == false) return;
		if (m_fadeFactor >= 1.0f)
		{	SetMenuVisible(true);
			m_isFading = false;
			m_isFadeActive = false;
			string mpath = GameManager.m_prefabPath+"/Cancel";
			if (GameManager.m_stackScene.Count>=2)
				GameManager.m_cancel = GameObject.Instantiate((GameObject)Resources.Load(mpath)) as GameObject;
		}
	}
	public float Mix(float x, float y, float a)
	{	return x*(1.0f-a)+y*a;
	}
	public Rect LerpRect(Rect a, Rect b, float f)
	{	float x = Mix (a.x, b.x, f);
		float y = Mix (a.y, b.y, f);
		float w = Mix (a.width, b.width, f);
		float h = Mix (a.height, b.height, f);
		return new Rect(x,y,w,h);
	}

	public void OnGUI()
	{
		if (m_isFadeActive==false) return;
		if (m_isFading==false) return;
		if (m_isGrabNewScene>0)	return;
		if (m_hasGrabNewScene>0) return;

		int w=Screen.width, h=Screen.height;
		int vw=w/10, vh=h/10;
		Rect rnormal = new Rect(0,0,w,h);
		Rect rsmall = new Rect(0+vw*4,0+vh*4, w-vw*8,h-vh*8);
		Rect rbig = new Rect(0-vw*4,0-vh*4, w+vw*8,h+vh*8);

		if (m_sens==true)
		{	m_recta = LerpRect(rnormal, rbig, m_fadeFactor);
			m_rectb = LerpRect(rsmall, rnormal, m_fadeFactor);

			GUI.color = new Color(1,1,1, 1.0f-m_fadeFactor);
			//GUI.DrawTexture (m_recta, m_texa);

			GUI.color = new Color(1,1,1, m_fadeFactor);
			//GUI.DrawTexture (m_rectb, m_texb);
		}
		else
		{	m_recta = LerpRect(rnormal, rsmall, m_fadeFactor);
			m_rectb = LerpRect(rbig, rnormal, m_fadeFactor);

			GUI.color = new Color(1,1,1, 1.0f-m_fadeFactor);
			//GUI.DrawTexture (m_recta, m_texa);

			GUI.color = new Color(1,1,1, m_fadeFactor);
			//GUI.DrawTexture (m_rectb, m_texb);
		}
		GUI.color = Color.white;

		if (m_isrtWarm == true)
		{	m_isrt = false;
			m_isrtWarm = false;
			//RenderTexture.active = null;
			SetMenuFadeRenderTarget(false);
		}
	}
}
