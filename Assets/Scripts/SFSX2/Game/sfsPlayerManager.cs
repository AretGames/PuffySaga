
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Spawns player and items objects, stores them in collections and provides access to them
public class sfsPlayerManager : MonoBehaviour
{
	public GameObject enemyPrefab;
	public GameObject playerPrefab;
	public GameObject ammoPrefab;
	public GameObject healthPrefab;
	
	public GameObject sparkPrefab;
	public GameObject bloodPrefab;
	
	
	private GameObject playerObj;
	
	private static sfsPlayerManager instance;
	public static sfsPlayerManager Instance
	{	get
		{	return instance;
		}
	}
	
	public GameObject GetPlayerObject()
	{	return playerObj;
	}
	
	private Dictionary<int, sfsNetworkTransformReceiver> recipients = new Dictionary<int, sfsNetworkTransformReceiver>();
	private Dictionary<int, GameObject> items = new Dictionary<int, GameObject>();
		
	void Awake()
	{
		instance = this;
	}

	public void SpawnItem(int id, sfsNetworkTransform ntransform, string itemType)
	{
		/*
		GameObject itemPrefab = null;
		
		if (itemType == "Ammo")
		{	itemPrefab = ammoPrefab;
		}
		else
		{	itemPrefab = healthPrefab;
		}
		
		GameObject itemObj = GameObject.Instantiate(itemPrefab) as GameObject;
		itemObj.transform.position = ntransform.Position;
		itemObj.transform.localEulerAngles = ntransform.AngleRotationFPS;
		items[id] = itemObj;
		*/
	}
	
	public void RemoveItem(int id)
	{	if (items.ContainsKey(id))
		{	Destroy(items[id]);
			items.Remove(id);
		}
	}

	public void SpawnPlayer(sfsNetworkTransform ntransform, string name, int score)
	{
		Debug.Log ("sfsPlayerManager:SpawnPlayer");
		/*
		if (Camera.main!=null)
		{
			Destroy(Camera.main.gameObject);
		}
		*/
		sfsGameHUD.Instance.UpdateHealth(100);
		/*
		playerObj = GameObject.Instantiate(playerPrefab) as GameObject;
		playerObj.transform.position = ntransform.Position;
		playerObj.transform.localEulerAngles = ntransform.AngleRotationFPS;
		*/
		playerObj = GameManager.SpawnPlayer("Puffy", ntransform.Position);
		playerObj.AddComponent<sfsNetworkTransformSender>();

		playerObj.SendMessage("StartSendTransform");
				
		sfsPlayerScore.Instance.SetScore(name, score);
	}
	
	public void SpawnEnemy(int id, sfsNetworkTransform ntransform, string name, int score)
	{
		Debug.Log ("sfsPlayerManager:SpawnEnemy");
		//SpawnPlayer (ntransform, name, score);
		//return;
		//GameObject playerObj = GameObject.Instantiate(enemyPrefab) as GameObject;
		GameObject playerObj = GameManager.SpawnEnemy("Puffy", ntransform.Position);
		playerObj.AddComponent<sfsNetworkTransformReceiver>();
		playerObj.AddComponent<sfsNetworkTransformInterpolation>();

		playerObj.transform.position = ntransform.Position;
		playerObj.transform.localEulerAngles = ntransform.AngleRotationFPS;
		//sfsAnimationSynchronizer animator = playerObj.GetComponent<sfsAnimationSynchronizer>();
		//animator.StartReceivingAnimation();
				
		//sfsPlayerScore.Instance.SetScore(name, score);
		
		//sfsEnemy enemy = playerObj.GetComponent<sfsEnemy>();
		//enemy.Init(name);

		recipients[id] = playerObj.GetComponent<sfsNetworkTransformReceiver>();
	}
	
	public sfsNetworkTransformReceiver GetRecipient(int id)
	{
		if (recipients.ContainsKey(id))
		{
			return recipients[id];
		}
		return null;
	}
	
	public void UpdateHealthForEnemy(int id, int health)
	{
		sfsNetworkTransformReceiver rec = GetRecipient(id);
		rec.GetComponent<sfsEnemy>().UpdateHealth(health);
		
		BloodEffect(rec.transform);

	}
	
	public void DestroyEnemy(int id)
	{
		sfsNetworkTransformReceiver rec = GetRecipient(id);
		if (rec == null) return;
		Destroy(rec.gameObject);
		recipients.Remove(id);
	}
	
	public void SyncAnimation(int id, string msg, int layer)
	{
		sfsNetworkTransformReceiver rec = GetRecipient(id);
		
		if (rec == null) return;
		
		if (layer == 0)
		{
			rec.GetComponent<sfsAnimationSynchronizer>().RemoteStateUpdate(msg);
		}
		else
		{	if (layer == 1)
			{	rec.GetComponent<sfsAnimationSynchronizer>().RemoteSecondStateUpdate(msg);
			}
		}
	}
	
	public void KillEnemy(int id)
	{
		sfsNetworkTransformReceiver rec = GetRecipient(id);
		if (rec == null) return;
		GameObject obj = rec.gameObject;
		
		BloodEffect (obj.transform);
		
		GameObject hero = obj.transform.FindChild("Hero").gameObject;
		hero.transform.parent = null;
		Destroy(obj);
		hero.transform.Rotate(Vector3.right*90);
		hero.GetComponent<Animation>().Stop();
		Destroy(hero, 10);
		
		recipients.Remove(id);
	}
	
	public void KillMe()
	{
		sfsGameHUD.Instance.UpdateHealth(0);
		if (playerObj == null) return;
		Camera.main.transform.parent = null;
		Destroy(playerObj);
		playerObj = null;
	}
		
	public void BloodEffect (Transform t)
	{
			GameObject blood = GameObject.Instantiate(bloodPrefab) as GameObject;
			blood.transform.position = t.position;
			blood.transform.rotation = Quaternion.LookRotation(playerObj.transform.position - t.position);
	}
	
	public void ShotEffect()
	{
		Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit, Mathf.Infinity))
		{	if (hit.transform.gameObject.layer != (int)sfsGameLayers.TargetLayer)
			{  // miss			
				GameObject spark = GameObject.Instantiate(sparkPrefab) as GameObject;
				spark.transform.position = hit.point;
				spark.transform.rotation = Quaternion.LookRotation(hit.normal);
			}
		}				
	}
}

