using UnityEngine;
using System.Collections;
using System;

// This class receive the updated transform from server for the remote player model
public class sfsNetworkTransformReceiver : MonoBehaviour
{
	Transform thisTransform;
	
	private sfsNetworkTransformInterpolation interpolator;
	private sfsAnimationSynchronizer animator;
	
	void Awake()
	{
		thisTransform = this.transform;
		animator = GetComponent<sfsAnimationSynchronizer>();
		interpolator = GetComponent<sfsNetworkTransformInterpolation>();
		if (interpolator!=null)
		{
			interpolator.StartReceiving();
		}
	}
		
	public void ReceiveTransform(sfsNetworkTransform ntransform)
	{
		if (interpolator!=null)
		{
			//Debug.Log ("NetworkTransformReceive: interpolator");
			// interpolating received transform
			interpolator.ReceivedTransform(ntransform);
		}
		else
		{	//Debug.Log ("NetworkTransformReceive: no interpolation");
			//No interpolation - updating transform directly
			thisTransform.position = ntransform.Position;
			// Ignoring x and z rotation angles
			thisTransform.localEulerAngles = ntransform.AngleRotationFPS;
		}	
	}
		
}
