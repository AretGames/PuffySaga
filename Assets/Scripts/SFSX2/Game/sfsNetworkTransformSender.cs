using UnityEngine;
using System.Collections;
using System;

// Sends the transform of the local player to server
public class sfsNetworkTransformSender : MonoBehaviour
{

	// We will send transform each 0.1 second.
	//To make transform synchronization smoother consider
	//writing interpolation algorithm instead of making smaller period.
	public static readonly float sendingPeriod = 0.1f; 
	
	private readonly float accuracy = 0.002f;
	private float timeLastSending = 0.0f;

	private bool send = false;
	private sfsNetworkTransform lastState;
	
	private Transform thisTransform;
	
	void Start()
	{
		thisTransform = this.transform;
		lastState = sfsNetworkTransform.FromTransform(thisTransform);
	}
		
	// We call it on local player to start sending his transform
	void StartSendTransform()
	{
		send = true;
	}
	
	void FixedUpdate()
	{ 
		if (send)
		{
			SendTransform();
		}
	}
	
	void SendTransform()
	{
		if (timeLastSending >= sendingPeriod)
		{	lastState = sfsNetworkTransform.FromTransform(thisTransform);
			sfsNetworkManager.Instance.SendTransform(lastState);
			timeLastSending = 0;
			return;
		}
		timeLastSending += Time.deltaTime;
	}
		
}
