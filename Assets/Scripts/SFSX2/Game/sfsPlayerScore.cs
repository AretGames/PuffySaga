
using System;
using System.Collections.Generic;
using UnityEngine;

// Scores of the players
public class sfsPlayerScore
{
	
	private sfsPlayerScore() {
	}
	
	private static sfsPlayerScore instance;
	
	public static sfsPlayerScore Instance {
		get {
			if (instance == null) {
				instance = new sfsPlayerScore();
			}
			return instance;
		}
	}
	
	private Dictionary<string, int> scores = new Dictionary<string, int>();
	public Dictionary<string, int> Scores {
		get {
			return scores;
		}
	}
	
		
	public void SetScore(string playerName, int score) {
		scores[playerName] = score;
	}
	
	
}
