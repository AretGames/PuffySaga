
using System;
using System.Collections;
using UnityEngine;

using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;
using Sfs2X.Logging;

// The Neywork manager sends the messages to server and handles the response.
public class sfsNetworkManager : MonoBehaviour
{
	private bool running = false;
	
	private static sfsNetworkManager instance;
	public static sfsNetworkManager Instance
	{	get
		{	return instance;
		}
	}
	
	private SmartFox smartFox;  // The reference to SFS client
	
	void Awake()
	{
		instance = this;	
	}
	
	public void Start()
	{
		Debug.Log ("NetworkManager::Start()");
		smartFox = sfsSmartFoxConnection.Connection;
		if (smartFox == null)
		{	//Application.LoadLevel("lobby");
			Messenger.Broadcast<bool>(EVENT.MENUFADE_INIT, false);
			Messenger.Broadcast< string > (EVENT.SCENE_LOAD, "sfsLobby");
			return;
		}	
		
		SubscribeDelegates();
		SendSpawnRequest();
		
		sfsTimeManager.Instance.Init();
		
		running = true;
	}
			
	// This is needed to handle server events in queued mode
	void FixedUpdate()
	{
		if (!running) return;
		smartFox.ProcessEvents();
	}
		
	private void SubscribeDelegates()
	{
		smartFox.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);
		smartFox.AddEventListener(SFSEvent.USER_EXIT_ROOM, OnUserLeaveRoom);
		smartFox.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
	}
	
	private void UnsubscribeDelegates()
	{
		smartFox.RemoveAllEventListeners();
	}
	
	/// <summary>
	/// Send the request to server to spawn my player
	/// </summary>
	public void SendSpawnRequest()
	{
		Debug.Log ("NetworkManager::SendSpawnRequest");
		Room room = smartFox.LastJoinedRoom;
		// Stores the transform values to SFSObject to send them to server
		ISFSObject ntrans = new SFSObject();
		sfsNetworkTransform trans = new sfsNetworkTransform();
		trans.Position = GameManager.m_spawnPos;
		trans.ToSFSObject(ntrans);
		//ExtensionRequest request = new ExtensionRequest("spawnMe", new SFSObject(), room);
		ExtensionRequest request = new ExtensionRequest("spawnMe", ntrans, room);
		smartFox.Send(request);
	}
	
	/// <summary>
	///  Send a request to shoot
	/// </summary>
	public void SendShot()
	{
		Room room = smartFox.LastJoinedRoom;
		ExtensionRequest request = new ExtensionRequest("shot", new SFSObject(), room);
		smartFox.Send(request);
	}
	
	/// <summary>
	/// Send a request to reload
	/// </summary>
	public void SendReload()
	{
		Room room = smartFox.LastJoinedRoom;
		ExtensionRequest request = new ExtensionRequest("reload", new SFSObject(), room);
		smartFox.Send(request);
	}
	
	/// <summary>
	/// Send local transform to the server
	/// </summary>
	/// <param name="ntransform">
	/// A <see cref="NetworkTransform"/>
	/// </param>
	public void SendTransform(sfsNetworkTransform ntransform)
	{
		Room room = smartFox.LastJoinedRoom;
		ISFSObject data = new SFSObject();
		ntransform.ToSFSObject(data);
		ExtensionRequest request = new ExtensionRequest("sendTransform", data, room, true); // True flag = UDP
		//ExtensionRequest request = new ExtensionRequest("sendTransform", data, room, false); // True flag = UDP
		smartFox.Send(request);
	}
	
	/// <summary>
	/// Send local animation state to the server
	/// </summary>
	/// <param name="message">
	/// A <see cref="System.String"/>
	/// </param>
	/// <param name="layer">
	/// A <see cref="System.Int32"/>
	/// </param>
	public void SendAnimationState(string message, int layer)
	{
		Room room = smartFox.LastJoinedRoom;
		ISFSObject data = new SFSObject();
		data.PutUtfString("msg", message);
		data.PutInt("layer", layer);
		ExtensionRequest request = new ExtensionRequest("sendAnim", data, room);
		smartFox.Send(request);
	}
	
	/// <summary>
	/// Request the current server time. Used for time synchronization
	/// </summary>	
	public void TimeSyncRequest()
	{
		Room room = smartFox.LastJoinedRoom;
		ExtensionRequest request = new ExtensionRequest("getTime", new SFSObject(), room);
		smartFox.Send(request);
	}
	
	/// <summary>
	/// When connection is lost we load the login scene
	/// </summary>
	private void OnConnectionLost(BaseEvent evt)
	{
		UnsubscribeDelegates();
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
		//Application.LoadLevel("lobby");
		Messenger.Broadcast<bool>(EVENT.MENUFADE_INIT, false);
		Messenger.Broadcast< string > (EVENT.SCENE_LOAD, "sfsLobby");
	}
	
	// This method handles all the responses from the server
	private void OnExtensionResponse(BaseEvent evt)
	{
		//string szcmd = (string)evt.Params["cmd"];
		//Debug.Log ("NetworkManager::OnExtensionResponse("+szcmd+")");
		try
		{
			string cmd = (string)evt.Params["cmd"];
			ISFSObject dt = (SFSObject)evt.Params["params"];
											
			if (cmd == "spawnPlayer")
			{	HandleInstantiatePlayer(dt);
			}
			else if (cmd == "transform")
			{	HandleTransform(dt);
			}
			else if (cmd == "notransform")
			{	HandleNoTransform(dt);
			}
			else if (cmd == "killed")
			{	HandleKill(dt);
			}
			else if (cmd == "health")
			{	HandleHealthChange(dt);
			}
			else if (cmd == "anim")
			{	HandleAnimation(dt);
			}
			else if (cmd == "score")
			{	HandleScoreChange(dt);
			}
			else if (cmd == "ammo")
			{	HandleAmmoCountChange(dt);
			}
			else if (cmd == "spawnItem")
			{	HandleItem(dt);
			}
			else if (cmd == "removeItem")
			{	HandleRemoveItem(dt);
			}
			else if (cmd == "enemyShotFired")
			{	HandleShotFired(dt);
			}
			else if (cmd == "time")
			{	HandleServerTime(dt);
			}
			else if (cmd=="reloaded")
			{	HandleReload(dt);
			}
		}
		catch (Exception e) {
			Debug.Log("Exception handling response: "+e.Message+" >>> "+e.StackTrace);
		}
		
	}
	
	// Instantiating player (our local FPS model, or remote 3rd person model)
	private void HandleInstantiatePlayer(ISFSObject dt)
	{
		Debug.Log ("HandleInstantiatePlayer");
		ISFSObject playerData = dt.GetSFSObject("player");
		int userId = playerData.GetInt("id");
		int score = playerData.GetInt("score");
		sfsNetworkTransform ntransform = sfsNetworkTransform.FromSFSObject(playerData);
						
		User user = smartFox.UserManager.GetUserById(userId);
		string name = user.Name;
		
		if (userId == smartFox.MySelf.Id)
		{
			sfsPlayerManager.Instance.SpawnPlayer(ntransform, name, score);
		}
		else
		{
			sfsPlayerManager.Instance.SpawnEnemy(userId, ntransform, name, score);
		}
	}
	
	// Updating transform of the remote player from server
	private void HandleTransform(ISFSObject dt)
	{
		int userId = dt.GetInt("id");
		sfsNetworkTransform ntransform = sfsNetworkTransform.FromSFSObject(dt);
		if (userId != smartFox.MySelf.Id)
		{
			// Update transform of the remote user object
		
			sfsNetworkTransformReceiver recipient = sfsPlayerManager.Instance.GetRecipient(userId);
			if (recipient!=null)
			{
				recipient.ReceiveTransform(ntransform);
			}
		}
	}
	
	// Server rejected transform message - force the local player object to what server said
	private void HandleNoTransform(ISFSObject dt)
	{
		int userId = dt.GetInt("id");
		sfsNetworkTransform ntransform = sfsNetworkTransform.FromSFSObject(dt);
		
		if (userId == smartFox.MySelf.Id)
		{
			// Movement restricted!
			// Update transform of the local object
			ntransform.Update(sfsPlayerManager.Instance.GetPlayerObject().transform);
		}
	}
	
	// Synchronize the time from server
	private void HandleServerTime(ISFSObject dt)
	{
		long time = dt.GetLong("t");
		sfsTimeManager.Instance.Synchronize(Convert.ToDouble(time));
	}
	
	// Handle player killed
	private void HandleKill(ISFSObject dt)
	{
		int userId = dt.GetInt("id");
		int killerId = dt.GetInt("killerId");
		
		if (userId != smartFox.MySelf.Id)
		{
			sfsPlayerManager.Instance.KillEnemy(userId);
			if (killerId == smartFox.MySelf.Id)
			{
				sfsSoundManager.Instance.PlayKillEnemy(sfsPlayerManager.Instance.GetPlayerObject().GetComponent<AudioSource>());
			}
		}
		else
		{
			sfsPlayerManager.Instance.KillMe();
		}
	}
	
	// Health of the player changed - updating GUI and playing sounds if it's damage
	private void HandleHealthChange(ISFSObject dt)
	{
		int userId = dt.GetInt("id");
		int health = dt.GetInt("health");
		if (userId == smartFox.MySelf.Id)
		{
			if (health<sfsGameHUD.Instance.Health)
			{
				sfsSoundManager.Instance.PlayDamage(sfsPlayerManager.Instance.GetPlayerObject().GetComponent<AudioSource>());
			}
			
			sfsGameHUD.Instance.UpdateHealth(health);
		}
		else
		{
			sfsPlayerManager.Instance.UpdateHealthForEnemy(userId, health);
		}
	}
	
	// Ammo count changed message from server
	private void HandleAmmoCountChange(ISFSObject dt)
	{
		/*
		int userId = dt.GetInt("id");
		
		if (userId != smartFox.MySelf.Id) return;
		
		int loadedAmmo = dt.GetInt("ammo");
		int maxAmmo = dt.GetInt("maxAmmo");
		int ammo = dt.GetInt("unloadedAmmo");
				
		sfsShotController.Instance.UpdateAmmoCount(loadedAmmo, maxAmmo, ammo);
		*/
	}
	
	// Score changed message from server
	private void HandleScoreChange(ISFSObject dt)
	{
		int userId = dt.GetInt("id");
		int score = dt.GetInt("score");
		
		User user = smartFox.UserManager.GetUserById(userId);
		if (user!=null)
		{
			string name = user.Name;
			sfsPlayerScore.Instance.SetScore(name, score);
		}
	}
	
	// New item spawned message. Instantiating the item object.
	private void HandleItem(ISFSObject dt)
	{
		ISFSObject item = dt.GetSFSObject("item");
		int id = item.GetInt("id");
		string itemType = item.GetUtfString("type");
		sfsNetworkTransform ntransform = sfsNetworkTransform.FromSFSObject(item);
		
		sfsPlayerManager.Instance.SpawnItem(id, ntransform, itemType);
	}
	
	// Removing item (hwen it was picked up by someone)
	private void HandleRemoveItem(ISFSObject dt)
	{
		int playerId = dt.GetInt("playerId");
		ISFSObject item = dt.GetSFSObject("item");
		int id = item.GetInt("id");
		string type = item.GetUtfString("type");
		if (playerId == smartFox.MySelf.Id)
		{
			if (type == "HealthPack")
			{
				sfsSoundManager.Instance.PlayPickupHealthPack(sfsPlayerManager.Instance.GetPlayerObject().GetComponent<AudioSource>());				
			} 
			else
			{	if (type == "Ammo")
				{
					sfsSoundManager.Instance.PlayPickupAmmo(sfsPlayerManager.Instance.GetPlayerObject().GetComponent<AudioSource>());							
				}
			}
		}
		
		sfsPlayerManager.Instance.RemoveItem(id);
	}
	
	// Synchronizing remote animation
	private void HandleAnimation(ISFSObject dt) {
		int userId = dt.GetInt("id");
		string msg = dt.GetUtfString("msg");
		int layer = dt.GetInt("layer");
		
		if (userId != smartFox.MySelf.Id)
		{
			sfsPlayerManager.Instance.SyncAnimation(userId, msg, layer);
		}
	}
	
	// When someon shots handle it and play corresponding animation 
	private void HandleShotFired(ISFSObject dt)
	{
		int userId = dt.GetInt("id");
		if (userId != smartFox.MySelf.Id)
		{
			sfsSoundManager.Instance.PlayShot(sfsPlayerManager.Instance.GetRecipient(userId).GetComponent<AudioSource>());
			sfsPlayerManager.Instance.SyncAnimation(userId, "Shot", 1);
		}
		else
		{
			GameObject obj = sfsPlayerManager.Instance.GetPlayerObject();
			if (obj == null) return;
						
			sfsSoundManager.Instance.PlayShot(obj.GetComponent<AudioSource>());
			obj.GetComponent<sfsAnimationSynchronizer>().PlayShotAnimation();
			
			sfsPlayerManager.Instance.ShotEffect();
		}
	}
	
	// When someone reloaded the weapon - play corresponding animation
	private void HandleReload(ISFSObject dt)
	{
		int userId = dt.GetInt("id");
		if (userId != smartFox.MySelf.Id)
		{
			sfsSoundManager.Instance.PlayReload(sfsPlayerManager.Instance.GetRecipient(userId).GetComponent<AudioSource>());
			sfsPlayerManager.Instance.SyncAnimation(userId, "Reload", 1);
		}
		else
		{
			GameObject obj = sfsPlayerManager.Instance.GetPlayerObject();
			if (obj == null) return;
			
			sfsSoundManager.Instance.PlayReload(obj.GetComponent<AudioSource>());
			obj.GetComponent<sfsAnimationSynchronizer>().PlayReloadAnimation();
		}
	}

	// When a user leaves room destroy his object
	private void OnUserLeaveRoom(BaseEvent evt)
	{
		User user = (User)evt.Params["user"];
		Room room = (Room)evt.Params["room"];
				
		sfsPlayerManager.Instance.DestroyEnemy(user.Id);
		Debug.Log("User "+user.Name+" left");
	}
	
	void OnApplicationQuit()
	{
		UnsubscribeDelegates();
	}
}
