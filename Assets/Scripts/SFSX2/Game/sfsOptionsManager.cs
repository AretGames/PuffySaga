using System;

public static class sfsOptionsManager {

	private static bool invertMouseY = false;
	
	public static bool InvertMouseY {
		get {
			return invertMouseY;
		}
		set {
			invertMouseY = value;
		}
	}
}
