﻿using UnityEngine;
using System.Collections;

namespace PuffySaga
{
	public class Stats
	{
		public float	m_endurance;	//nombre de point de vies maximum
		public float	m_armure;		//resistance aux degats
		public float	m_force;		//degats physiques
		public float	m_vitesse;		//vitesse de deplacement et d'attaque
		public float	m_magie;		//puissance des sorts
		public float	m_agilité;		//probabilité d'esquive / chances de coups critiques

		public void Reset()
		{	m_endurance = 0;
			m_armure = 0;
			m_force = 0;
			m_vitesse = 0;
			m_magie = 0;
			m_agilité = 0;
		}
	}

	public struct _stats
	{	
		public Stats	current;		//characteristiques actuels du personnage
		public Stats	initial;		//characteristiques de base du personnage
	};
}