﻿using UnityEngine;
using System.Collections;

public class MenuScreen : MonoBehaviour
{
	public void OnClickedButton(string nextscene)
	{
		Messenger.Broadcast<bool>(EVENT.MENUFADE_INIT, true);
		Messenger.Broadcast< string > (EVENT.SCENE_LOAD, nextscene);	//load selected scene

	}
	public void OnClickedBack()
	{
		Messenger.Broadcast(EVENT.MENU_BACK);
	}

}
