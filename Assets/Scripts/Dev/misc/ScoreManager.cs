﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {
	
	public float SPRITE_WIDTH = 0.83f;
	public float m_posx = 0.5f;
	public float m_posy = 0.9f;

	private const string SPRITE_TAG = "SCORE_SPRITE";   //http://answers.unity3d.com/questions/33597/is-it-possible-to-create-a-tag-programmatically.html
	private Camera m_camera;

	void Start() {
		GameObject camobj = GameObject.Find ("/UiCamera");
		m_camera = camobj.GetComponent<Camera> ();
	}
	
	void Update() {
		UpdateScore (2450);
	}

	public void UpdateScore (int score) {
		
		//remove previous scores
		GameObject[] oldScoreDigits = GameObject.FindGameObjectsWithTag(SPRITE_TAG);
		foreach (var oldScoreDigit in oldScoreDigits) {
			Destroy(oldScoreDigit);
		}
		
		string scoreStr = score.ToString();
		
		for (int i = 0; i < scoreStr.Length; i++)
		{
			GameObject digit = (GameObject)Instantiate(GameObject.Find(scoreStr.Substring(i,1)));
			
			digit.tag = SPRITE_TAG;   //tag this object for easy removal
			digit.transform.parent = this.transform;
			digit.transform.localPosition = new Vector3(i * SPRITE_WIDTH, 0, transform.position.z);                
		}
		
		//center score to top screen
		if (m_camera != null) {
			Vector3 pos = m_camera.ScreenToWorldPoint (new Vector3 (Screen.width *m_posx, Screen.height * m_posy, m_camera.nearClipPlane));
			pos.x -= scoreStr.Length * SPRITE_WIDTH / 2f; 
			transform.position = pos;
		}
	}
}