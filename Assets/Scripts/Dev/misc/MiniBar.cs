﻿using UnityEngine;
using System.Collections;

public class MiniBar : MonoBehaviour {

	public float	m_life=10;
	public float	m_power=10;
	GameObject		m_obj=null;
	Rect			m_rc = new Rect();
	public Vector2	m_barsize = new Vector2(100,20);
	public Vector2	m_baroffset = new Vector2(0,-128);
	public GUIStyle m_barStyle;
	public Color	m_colorHealthEmpty;
	public Color	m_colorHealthMiddle;
	public Color	m_colorHealthFull;
	Camera			m_camera = null;
	bool			m_isVisible;
	//debug
	public float	m_lifedir=1;

	// Use this for initialization
	void Start ()
	{
		m_obj = gameObject;
		m_barStyle = new GUIStyle();
		m_barStyle.normal.background = Resources.Load ("white") as Texture2D;
		m_barStyle.normal.textColor = new Color (1, 1, 1);
		m_barStyle.fontSize = 17;
		m_barStyle.fontStyle = FontStyle.Bold;
		m_barStyle.alignment = TextAnchor.MiddleCenter;
		m_colorHealthEmpty = new Color(1,0,0);
		m_colorHealthMiddle = new Color(2,2,0);
		m_colorHealthFull = new Color(0,1,0);
		m_isVisible = true;
		Messenger.AddListener< Camera >(EVENT.CAMERA_RECEIVED, OnCameraReceived );
		Messenger.AddListener< float >(EVENT.UPDATE, OnUpdate );
		Messenger.AddListener< string >(EVENT.MENU_ISOPENED, OnMenuIsOpened );
		Messenger.AddListener< string >(EVENT.MENU_ISCLOSED, OnMenuIsClosed );
	}
	void OnMenuIsOpened(string menuname)
	{	m_isVisible = false;
	}
	void OnMenuIsClosed(string menuname)
	{	m_isVisible = true;
	}
	void OnDisable()
	{
		Messenger.RemoveListener< Camera >(EVENT.CAMERA_RECEIVED, OnCameraReceived );
		Messenger.RemoveListener< float >(EVENT.UPDATE, OnUpdate );
		Messenger.RemoveListener< string >(EVENT.MENU_ISOPENED, OnMenuIsOpened );
		Messenger.RemoveListener< string >(EVENT.MENU_ISCLOSED, OnMenuIsClosed );
	}

	void OnCameraReceived(Camera cam)
	{	m_camera = cam;
	}

	// Update is called once per frame
	void OnUpdate (float elapsedTime)
	{
		//temporaries test
		if (m_life <= 0)
			m_lifedir = 1;
		if (m_life >= 100)
			m_lifedir = -1;
		m_life += m_lifedir * 10 * elapsedTime;
	}

	void OnGUI() {
		if (m_obj != null)
		{
			Camera cam = m_camera;
			if (cam == null)
			{	Messenger.Broadcast(EVENT.CAMERA_GET);
				return;
			}
			if (m_isVisible == false)
				return;
			m_life = Mathf.Max ( Mathf.Min (m_life,100.0f), 0.0f);
			Vector2 sc = cam.WorldToScreenPoint(m_obj.transform.position);
			float r = 1.0f;//(2.5f / cam.orthographicSize) * (Screen.width/Screen.height);
			float bsx = m_barsize.x*r;
			float bsy = m_barsize.y*r;
			float box = m_baroffset.x*r;
			float boy = m_baroffset.y*r;
			int h = Screen.height;
			m_rc.Set((sc.x-bsx*0.5f)+box+2*r, h-((sc.y-bsy*0.5f)-boy-2*r), Mathf.Max (bsx*m_life/100.0f-4*r,0.0f), bsy-4*r);
			Color cstart,cend;
			if (m_life <=50)
			{	cstart=m_colorHealthEmpty; cend=m_colorHealthMiddle;
			}
			else
			{	cstart=m_colorHealthMiddle; cend=m_colorHealthFull;
			}
			GUI.backgroundColor = Color.Lerp (cstart, cend, Mathf.Min ((m_life)/100.0f, 1.0f));
			GUI.Box (m_rc, "" + (int)m_life + "%", m_barStyle);

			m_rc.Set((sc.x-bsx*0.5f)+box, h-((sc.y-bsy*0.5f)-boy), bsx, bsy);
			//GUI.Box (m_rc, "" + (int)m_life + "%");

		}
	}

}
