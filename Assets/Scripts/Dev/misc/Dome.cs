﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR

[ExecuteInEditMode]
#endif //UNITY_EDITOR

public class Dome : MonoBehaviour
{

	//
	[SerializeField]
	private int		m_domeLights=8;
	[SerializeField]
	private float	m_domeRadius=5;
	[SerializeField]
	private float	m_domeIntensity=1.0f;

	private bool	m_alreadyCreated = false;
	private bool	m_doCreate = false;

	#if UNITY_EDITOR
	public int domeLights
	{	get { return m_domeLights; }
		set
		{	m_domeLights = value;
			m_doCreate = true;
		}
	}
	public float domeRadius
	{	get { return m_domeRadius; }
		set
		{	m_domeRadius = value;
			m_doCreate = true;
		}
	}
	public float domeIntensity
	{	get { return m_domeIntensity; }
		set
		{	m_domeIntensity = value;
			m_doCreate = true;
		}
	}
	#endif //UNITY_EDITOR

	//-------------------------------
	[SerializeField]
	private float		m_ajr=63.77f;
	[SerializeField]
	private float		m_salaire=1000.0f;
	[SerializeField]
	private float		m_revenu=0.0f;
	public float ajr
	{	get { return m_ajr; }
		set
		{	m_ajr = value;
			ComputeRevenu();
		}
	}
	public float salaire
	{	get { return m_salaire; }
		set
		{	m_salaire = value;
			ComputeRevenu();
		}
	}
	void ComputeRevenu()
	{	float ab = (m_ajr * 30.0f);
		float p = ab - (m_salaire * 70.0f) / 100.0f;
		m_revenu = salaire + p;
		Debug.Log (m_revenu);
	}
	//-------------------------------
	// Use this for initialization
	void Start ()
	{
		#if UNITY_EDITOR
		if (m_alreadyCreated == true)
			return;
		#endif //UNITY_EDITOR
		CreateDome ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (m_doCreate==true)
		{
			m_doCreate=false;
			CreateDome();
		}

	}
	//

	#if UNITY_EDITOR
	void OnValidate()
	{
		m_alreadyCreated = false;
		m_doCreate = true;
		ComputeRevenu ();
	}

	void OnInspectorUpdate()
	{
		if (m_doCreate==true)
		{
			m_doCreate=false;
			CreateDome();
		}
		ComputeRevenu ();
	}
	#endif //UNITY_EDITOR
	

	void CreateDome()
	{
		int nbobj = gameObject.transform.childCount;
		GameObject[] todel = new GameObject[nbobj];
		for (int i=0; i<nbobj; i++)
			todel[i] = gameObject.transform.GetChild (i).gameObject;
		for (int i=0; i<nbobj; i++)
			DestroyImmediate (todel[i]);
		
		//create defaut dome lights
		int maxlight = m_domeLights;
		float PI = 3.14159265f;

		for (int i=0; i<maxlight; i++)
		{	GameObject lightNode = new GameObject("domeLight"+i);
			lightNode.transform.parent = gameObject.transform;

			//float a = (((float)i / maxlight) * 360.0f) * (3.14159265f / 180.0f);
			//float x = Mathf.Cos(a);
			//float z = Mathf.Sin(a);
			//Vector3 pos = new Vector3(x,1,z);
			float azimuthal = 2*PI*Random.Range (0.0f,1.0f);
			float zenith = Mathf.Asin(Random.Range (0.0f,1.0f));
			
			// Calculate the cartesian point
			float x = Mathf.Sin(zenith)*Mathf.Cos(azimuthal); 
			float y = Mathf.Sin(zenith)*Mathf.Sin(azimuthal);
			float z = Mathf.Cos(zenith);

			lightNode.transform.position = new Vector3(x,z,y) * m_domeRadius;
			lightNode.transform.LookAt(Vector3.zero);
			Light light = lightNode.AddComponent<Light>();
			light.type = LightType.Directional;
			light.intensity = (1.0f / (float)maxlight) * m_domeIntensity;
			light.range = m_domeRadius*1.5f;
			light.shadows = LightShadows.Hard;
			light.bounceIntensity = 0;
		}

		#if UNITY_EDITOR
		m_alreadyCreated = true;
		#endif //UNITY_EDITOR
	}
}
