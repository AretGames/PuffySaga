﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;

[ExecuteInEditMode]
#endif //UNITY_EDITOR

public class EdScene : MonoBehaviour
{

	//
	[SerializeField]
	private string	m_edSceneName="";

	private bool	m_sheduleLoadEdScene=false;

	#if UNITY_EDITOR
	public string edSceneName
	{
		get { return m_edSceneName; }
		set
		{	m_edSceneName = value;
			m_sheduleLoadEdScene = true;
		}
	}
	#endif //UNITY_EDITOR
	public bool				m_alreadyCreated = false;
	bool					m_hasTilemap;
	int						m_freeAtlasSlot;

	List<string>			m_totalTileList;

	TilesParam				m_tp = new TilesParam();

	// Use this for initialization
	void Start ()
	{
		#if UNITY_EDITOR
		if (m_alreadyCreated == true)
			return;
		#endif //UNITY_EDITOR
		
		LoadEdScene ();

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (m_sheduleLoadEdScene==true)
		{
			m_sheduleLoadEdScene=false;
			LoadEdScene();
		}

	}
	//
	#if UNITY_EDITOR
	void OnValidate()
	{
		m_alreadyCreated = false;
		m_sheduleLoadEdScene = true;
	}
	void OnInspectorUpdate()
	{
		if (m_sheduleLoadEdScene==true)
		{
			m_sheduleLoadEdScene=false;
			LoadEdScene();
		}
	}
	#endif //UNITY_EDITOR
	

	void LoadEdScene()
	{
		if (m_edSceneName.Length < 1)
			return;
		
		//m_edSceneName = "ABC";	//debug
		m_hasTilemap = false;
		m_freeAtlasSlot = 0;

		int nbobj = gameObject.transform.childCount;
		GameObject[] todel = new GameObject[nbobj];
		for (int i=0; i<nbobj; i++)
			todel[i] = gameObject.transform.GetChild (i).gameObject;
		for (int i=0; i<nbobj; i++)
			DestroyImmediate (todel[i]);

		string edscenename = GameManager.m_edscenePath + "/" + m_edSceneName;
		TextAsset txtdata = Resources.Load (edscenename) as TextAsset;
		if (txtdata == null)
			return;

		m_totalTileList = new List<string>();
		m_totalTileList.Clear ();

		
		MemoryStream ms = new MemoryStream (txtdata.bytes);
		StreamReader sr = new StreamReader (ms);
		
		string words = sr.ReadToEnd ();
		string [] split = words.Split (new Char [] {' ', ',', '.', ':', '\t', '\r', '\n', '\"' });
		int k = 0;

		GameObject node = gameObject;

		for (;;)
		{
			if (k >= split.Length)
				break;
			string str = split [k++];
			if (str.Length < 1)
				continue;
			if (str == "SCN")
				continue;
			if (str == "Node")
			{
				LoadNode (ref node, ref split, ref k);
				continue;
			}
		}
		if (m_hasTilemap == true)
		{
			//create atlas
			m_tp.m_tilesetSize = 2048;
			m_tp.m_atlas = new Texture2D(m_tp.m_tilesetSize, m_tp.m_tilesetSize);

			//get tileset textures used
			m_totalTileList.Clear ();
			for (int i=0; i<node.transform.childCount; i++)
			{	GameObject obj = node.transform.GetChild (i).gameObject;
				EdTilemap[] tms = obj.GetComponents<EdTilemap>();
				if (tms==null) continue;
				for (int t=0; t<tms.Length; t++)
				{	EdTilemap tm = tms[t];
					for (int j=0; j<tm.m_levelTileList.Count; j++)
						m_totalTileList.Add ( tm.m_levelTileList[j] );
				}
			}
			//fill atlas
			Texture2D[] atlasTextures = new Texture2D[m_totalTileList.Count];
			for (int i=0; i<m_totalTileList.Count; i++)
			{
				string tname = m_totalTileList[i];
				tname = tname.Replace ("Assets/Resources/", "");
				tname = tname.Replace ("Resources/", "");
				tname = tname.Replace ("Assets/", "");
				tname = GameManager.m_texturePath + "/" + tname;
				//atlasTextures[i] = Resources.Load(tname) as Texture2D;
				atlasTextures[i] = Resources.Load(tname, typeof(Texture2D)) as Texture2D;
				/*
				// <-_->
				#if UNITY_EDITOR
				string imgAssetPath = AssetDatabase.GetAssetPath(atlasTextures[i]);
				TextureImporter importer = AssetImporter.GetAtPath (imgAssetPath) as TextureImporter;
				importer.isReadable = true;
				AssetDatabase.ImportAsset(imgAssetPath, ImportAssetOptions.ForceUpdate );
				Debug.Log (imgAssetPath);
				#endif //UNITY_EDITOR
				// <-_->
				*/

			}
			m_tp.m_rects = m_tp.m_atlas.PackTextures(atlasTextures, 0, m_tp.m_tilesetSize, true);
			m_tp.m_tilesetSize = m_tp.m_atlas.width;
			m_tp.m_atlas.filterMode = FilterMode.Point;
			
			for (int i=0; i<node.transform.childCount; i++)
			{	GameObject obj = node.transform.GetChild (i).gameObject;
				EdTilemap[] tms = obj.GetComponents<EdTilemap>();
				if (tms==null) continue;
				for (int t=0; t<tms.Length; t++)
				{	EdTilemap tm = tms[t];
					GameObject tmobj = tm.CreateTilemap (obj, m_edSceneName, m_tp);
					tmobj.transform.parent = node.transform;
				}
			}

			#if UNITY_EDITOR
				m_alreadyCreated = true;
			#endif //UNITY_EDITOR
		}
	}

	//------------------
	void LoadNode(ref GameObject parent, ref string[] split, ref int k)
	{
		GameObject node = new GameObject ();
		node.transform.parent = parent.transform;

		for (;;)
		{
			if (k >= split.Length)
				break;
			string str = split [k++];
			if (str.Length < 1)
				continue;
			if (str == "EndNode")
				break;
			if (str == "component")
			{	LoadComponent(ref node, ref split, ref k);
				continue;
			}
			node.name = str;
		}
	}

	void LoadComponent(ref GameObject node, ref string[] split, ref int k)
	{
		for (;;)
		{
			if (k >= split.Length)
				break;
			string str = split [k++];
			if (str.Length < 1)
				continue;
			if (str == "NodeFlags")
			{	LoadComponentNodeFlags(ref node, ref split, ref k);
				return;
			}
			if (str == "Transform")
			{	LoadComponentTransform(ref node, ref split, ref k);
				return;
			}
			if (str == "EdTileMap")
			{	m_hasTilemap = true;
				EdTilemap tm = node.AddComponent<EdTilemap>();
				tm.LoadComponentEdTileMap(ref node, ref split, ref k, m_freeAtlasSlot);
				m_freeAtlasSlot += tm.m_levelTileList.Count;
				return;
			}
			break;
		}
	}

	void LoadComponentNodeFlags(ref GameObject node, ref string[] split, ref int k)
	{
	}

	void LoadComponentTransform(ref GameObject node, ref string[] split, ref int k)
	{
		Vector3 pos = new Vector3 ();
		Vector3 rot = new Vector3 ();
		Vector3 scl = new Vector3 ();
		string str = NextString(ref split, ref k);	//pos:
		str = split[k++];
		Single.TryParse (str, out pos.x);
		str = split[k++];
		Single.TryParse (str, out pos.y);
		str = split[k++];
		Single.TryParse (str, out pos.z);
		str = NextString(ref split, ref k);	//rot:
		str = split[k++];
		Single.TryParse (str, out rot.x);
		str = split[k++];
		Single.TryParse (str, out rot.y);
		str = split[k++];
		Single.TryParse (str, out rot.z);
		str = split[k++];
		str = NextString(ref split, ref k);	//scl:
		str = split[k++];
		Single.TryParse (str, out scl.x);
		str = split[k++];
		Single.TryParse (str, out scl.y);
		str = split[k++];
		Single.TryParse (str, out scl.z);
		str = split[k++];
		node.transform.position = pos;
		node.transform.rotation = Quaternion.Euler(rot);
		node.transform.localScale = scl;
	}


	string NextString(ref string [] split, ref int k)
	{
		string str;
		for(;;)
		{	if (k>=split.Length) return "";
			str = split[k++];
			if (str.Length>0) return str;
		}
	}
}
