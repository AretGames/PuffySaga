﻿using System;
using UnityEngine;
using System.Collections;

public static class Scheduler
{
	private static ArrayList ScheduledEvents = new ArrayList();
	private static object ThreadLocker = new object();
	
	static Scheduler()
	{
		Messenger.AddListener< float >(EVENT.UPDATE, OnUpdate );
	}
	static void OnUpdate(float elapsedTime)
	{
		ExecuteSchedule ();
	}

	public static void RegisterEvent(float ScheduledIn, FunctionPointer functionPointer) {
		ScheduledEvents.Add(new ScheduledEvent(functionPointer, ScheduledIn));
	}
	public static void RegisterEvent(float ScheduledIn, ParamiterizedFunctionPointer functionPointer, params object[] paramiters) {
		ScheduledEvents.Add(new ScheduledEvent(functionPointer, paramiters, ScheduledIn));
	}
	public static void ExecuteSchedule() {
		//Lock the thread so we dont overrun
		lock(ThreadLocker) {
			//Only if we have events because we need to initalize a control group
			if(ScheduledEvents.Count > 0) {
				//control group records the execute events for remove later
				ArrayList executedEvents = new ArrayList();
				
				//Test all our events
				foreach(ScheduledEvent nEvent in  ScheduledEvents) {
					if(System.DateTime.Now >= nEvent.TargetTime) {
						executedEvents.Add(nEvent);
						if(nEvent.xPointer != null) {
							nEvent.xPointer();
						} else if(nEvent.yPointer != null) {
							nEvent.yPointer(nEvent.Paramiters);
						}
					}
				}
				
				//remove the executed events
				foreach(ScheduledEvent nEvent in executedEvents) {
					ScheduledEvents.Remove(nEvent);
				}
				
				executedEvents.Clear();
			}
		}
	}
}

public class ScheduledEvent {
	/// <summary>
	/// The x pointer indicats a paramiterless function call.
	/// </summary>
	public FunctionPointer xPointer;
	/// <summary>
	/// The y pointer indicates a paramiterized function call.
	/// </summary>
	public ParamiterizedFunctionPointer yPointer;
	/// <summary>
	/// The target time to exectute the call.
	/// </summary>
	public DateTime TargetTime;
	/// <summary>
	/// The paramiters to use for paramiterized calls.
	/// </summary>
	public object[] Paramiters;
	/// <summary>
	/// Initializes a new instance of the <see cref="DarkStarGames.ScheduledEvent"/> class.
	/// </summary>
	/// <param name='pointer'>
	/// Pointer.
	/// </param>
	/// <param name='time'>
	/// Time.
	/// </param>
	public ScheduledEvent(FunctionPointer pointer, float time) {
		xPointer = pointer;
		TargetTime = DateTime.Now.AddMilliseconds(time);
	}
	/// <summary>
	/// Initializes a new instance of the <see cref="DarkStarGames.ScheduledEvent"/> class.
	/// </summary>
	/// <param name='pointer'>
	/// Pointer.
	/// </param>
	/// <param name='parmaiters'>
	/// Parmaiters.
	/// </param>
	/// <param name='time'>
	/// Time.
	/// </param>
	public ScheduledEvent(ParamiterizedFunctionPointer pointer, object[] parmaiters, float time) {
		this.yPointer = pointer;
		this.TargetTime = DateTime.Now.AddMilliseconds(time);
		this.Paramiters = parmaiters;
	}
}
/// <summary>
/// Function pointer.
/// </summary>
public delegate void FunctionPointer();
/// <summary>
/// Paramiterized function pointer.
/// </summary>
public delegate void ParamiterizedFunctionPointer(params object[] paramiters);

//usage:
// Scheduler.RegisterEvent(3000, new FunctionPointer(TestFunction));
// Scheduler.RegisterEvent(6000, new ParamiterizedFunctionPointer(paramTestFunction), "with paramiters to");

//add this line in your update:
// Scheduler.ExecuteSchedule();
