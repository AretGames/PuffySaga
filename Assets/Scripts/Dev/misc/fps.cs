﻿using UnityEngine;
using System.Collections;

public class fps : MonoBehaviour {
	const int		MAX_FPS = 120;
	float			m_frameCount = 0;
	float			m_dt = 0.0f;
	float			m_fps = 0.0f;
	float[]			m_totfps = new float[MAX_FPS];
	int				m_fpscounter = 0;
	float			m_fpsmoyen;
	public GUIStyle	m_textStyle;

	// Use this for initialization
	void Start ()
	{
		for (int i=0; i<MAX_FPS; i++)
			m_totfps [i] = 0.0f;
		m_fpscounter = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		m_frameCount++;
		m_dt += Time.deltaTime;

		if (m_dt > 1.0f) {
			m_fps = m_frameCount;
			m_frameCount = 0;
			m_dt -= 1.0f;
		}
		m_totfps [m_fpscounter++] = m_fps;
		if (m_fpscounter >= MAX_FPS)
			m_fpscounter = 0;
		m_fpsmoyen = 0;
		for (int i=0; i<MAX_FPS; i++)
			m_fpsmoyen += m_totfps [i];
		m_fpsmoyen /= MAX_FPS;
	}

	void OnGUI()
	{
		m_textStyle.fontSize = 56;
		m_textStyle.normal.textColor = Color.white;
		GUI.Label(new Rect( Screen.width/2,5, 30,30),""+(int)m_fpsmoyen,m_textStyle);
	}
}
