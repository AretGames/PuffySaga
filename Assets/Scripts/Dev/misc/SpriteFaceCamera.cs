﻿using UnityEngine;
using System;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;

[ExecuteInEditMode]
#endif //UNITY_EDITOR
public class SpriteFaceCamera : MonoBehaviour {

	//public static Action EditorUpdate;
	public Camera m_camera;
	
	// Use this for initialization
	void Start () {
		#if UNITY_EDITOR
			if (Camera.current == null) return;
			m_camera = Camera.current.GetComponent<Camera>();
		#else // UNITY_EDITOR
			GameObject camobj = GameObject.Find ("/Cameraman");
			m_camera = camobj.GetComponent<Camera> ();
		#endif // UNITY_EDITOR
		FaceCamera ();
	}
	
	#if UNITY_EDITOR
		private void CallbackFunction() {
			if (Camera.current != null) {
				m_camera = Camera.current.GetComponent<Camera> ();
				FaceCamera ();
			}
		}

		void OnEnable() {
			EditorApplication.update += CallbackFunction;
		}
		
		void OnDisable() {
			EditorApplication.update -= CallbackFunction;
		}
	#endif // UNITY_EDITOR

	// Update is called once per frame
	void Update () {
		#if UNITY_EDITOR
			if (Application.isPlaying) {
				GameObject camobj = GameObject.Find ("/Cameraman");
				if (camobj) {
					m_camera = camobj.GetComponent<Camera> ();
				} else {
					if (Camera.current == null) return;
					m_camera = Camera.current.GetComponent<Camera>();
				}
			} else {
				if (Camera.current == null) return;
				m_camera = Camera.current.GetComponent<Camera>();
			}

			if (m_camera == null) return;
		#endif // UNITY_EDITOR
		FaceCamera ();
	}

	void FaceCamera()
	{
		if(m_camera.orthographic)
			transform.LookAt(transform.position - m_camera.transform.forward, m_camera.transform.up);
		else   
			transform.LookAt(m_camera.transform.position, m_camera.transform.up);
	}
}
