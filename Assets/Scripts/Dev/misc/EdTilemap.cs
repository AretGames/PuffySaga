﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

public struct tilemap_header
{	public int		magic;			//= "TLMB" (tilemap binary)
	public int		levelSizeX;
	public int		levelSizeY;
	public int		tileSize;
	public int		tilesetSize;
};

public struct TileCornerMap
{	public int		x;
	public int		y;
	public byte		bit;
};

public class EdTilemap : MonoBehaviour
{
	Vector2 		m_levelSize = new Vector2(128,64);
	public float	m_unitSize;// = 1.0f;
	float			m_tileSize;// = 64;//128;
	byte[]			m_levelData = null;
	GameObject		m_tilemap;
	tilemap_header	head;

	//new functionnality
	public List<string>		m_levelTileList;
	int						m_freeAtlasSlot;

	readonly TileCornerMap[] tbit = new TileCornerMap[]
	{
		new TileCornerMap{ x= 1,y= 1,bit=0x01},
		new TileCornerMap{ x=-1,y= 1,bit=0x02},
		new TileCornerMap{ x= 0,y= 1,bit=0x03},
		new TileCornerMap{ x= 1,y=-1,bit=0x04},
		new TileCornerMap{ x= 1,y= 0,bit=0x05},
		new TileCornerMap{ x=-1,y=-1,bit=0x08},
		new TileCornerMap{ x=-1,y= 0,bit=0x0A},
		new TileCornerMap{ x= 0,y=-1,bit=0x0C},
	};

	List<byte> m_tablerand = new List<byte> ();

	string NextString(string [] split, ref int k)
	{
		string str;
		for(;;)
		{	if (k>=split.Length) return "";
			str = split[k++];
			if (str.Length>0) return str;
		}
	}

	public GameObject CreateTilemap(GameObject mapobj, string levelname, TilesParam tp)
	{
		int tw=(int)m_levelSize.x, th=(int)m_levelSize.y;
		float px = m_unitSize;
		float py = m_unitSize;
		//float r = 1.0f/tp.m_tilesetSize;

		if (m_tablerand.Count != tw*th)
		{	m_tablerand.Clear ();
			for (int i=0; i<tw*th; i++)
			{	m_tablerand.Add ( (byte)( UnityEngine.Random.Range (0, 16+5*1)));
			}
		}
		//int division = 32;
		//calcul du nombre de gameobject necessaire
		//int ow = division;
		//int oh = division;
		int ow = 256;	//debug test
		int oh = 42;	//debug test
		float fnbx = tw / (float)ow, fnby = th / (float)oh;

		int nbobjx = ((int)fnbx) + ((fnbx > (int)fnbx) ? 1 : 0);
		int nbobjy = ((int)fnby) + ((fnby > (int)fnby) ? 1 : 0);
		//
		//Material dmat = new Material( Shader.Find("Diffuse") );
		Material dmat = Resources.Load (GameManager.m_edscenePath + "/tilemap") as Material;
		dmat.SetTexture ("_MainTex", tp.m_atlas);
		dmat.renderQueue = 0;

		//creation des meshes
		Mesh mesh = null;

		int nbvtx = ow*oh*6;
		int nbindex = ow*oh * 6;
		Vector3[] vertices = new Vector3[nbvtx];
		Vector3[] normals = new Vector3[nbvtx];
		Vector2[] uvs1 = new Vector2[nbvtx];
		int[] triangles = new int[nbindex];

		int vidx = 0;
		int uidx = 0;
		int nidx = 0;
		uint tri_index = 0;

		int nblayer = m_levelTileList.Count;

		for (int ilayer=0; ilayer<nblayer; ilayer++)
		{
			//if (ila!=24) continue;
			int ila = ilayer;
			int imslot = ila + m_freeAtlasSlot;
			bool hasDetail = (tp.m_rects[imslot].width > tp.m_rects[imslot].height) ? true : false;
			float lay = 0;//ila*-0.102f;
			m_tileSize = (tp.m_rects[imslot].height*tp.m_atlas.height)/4;

			GameObject layerobj = new GameObject(mapobj.name + "_layer" + ila);
			layerobj.transform.parent = mapobj.transform;
			layerobj.transform.localPosition = Vector3.zero;

			float ustart = tp.m_rects[imslot].x, vstart=tp.m_rects[imslot].y;
			float uw = 1.0f / (tp.m_atlas.width / m_tileSize);
			float uh = 1.0f / (tp.m_atlas.height / m_tileSize);

			for (int ioby=0; ioby<nbobjy; ioby++)
			{	int starty = ioby*oh;
				for (int iobx=0; iobx<nbobjx; iobx++)
				{	mesh = CreateTilemapZone(ref layerobj, ioby*nbobjx+iobx, ref dmat, ilayer);
					vidx = 0;
					uidx = 0;
					nidx = 0;
					tri_index = 0;
					int startx = iobx*ow;
					for(int ty=starty; ty<starty+oh; ty++)
					{	if (ty >= th) break;
						for(int tx=startx; tx<startx+ow; tx++)
						{	if (tx >= tw) break;
							byte rlay = m_levelData[ty*tw+tx];
							byte bit=0;

							if (rlay != ila+1)
							{	//find corner
								for (int ci=0; ci<8; ci++)
								{	int cy=tbit[ci].y + ty;
									int cx=tbit[ci].x + tx;
									if (cx<0 || cy<0 || cx>=tw || cy>=th) continue;
									byte crlay = m_levelData[cy*tw+cx];
									if (crlay == ila+1)
										bit |= tbit[ci].bit;
								}
								if (bit==0) continue;
							}

							//select detail
							float detailstart = 0;
							if ((bit==0 || bit==0x0F) && hasDetail==true)
							{	bit = m_tablerand[ty*tw+tx];
								if (bit<16)
									detailstart = uw*4;
								else
									bit = 0;
							}
							
							float tu = (float)((bit%4) * uw);
							float tv = (float)((3-(bit/4)) * uh);
							
							float u0 = ustart + tu + detailstart;
							float v0 = vstart + tv;
							float u1 = u0 + uw;
							float v1 = v0 + uh;
							float x = (tx - tw/2)*m_unitSize;
							float y = (ty - th/2)*m_unitSize;

							//1st triangle
							vertices [vidx++] = new Vector3(x+0, lay, y+py);
							normals [nidx++] = new Vector3(0,1,0);
							uvs1 [uidx++] = new Vector2(u0, v0);
							vertices [vidx++] = new Vector3(x+px, lay, y+0);
							normals [nidx++] = new Vector3(0,1,0);
							uvs1 [uidx++] = new Vector2(u1, v1);
							vertices [vidx++] = new Vector3(x+0, lay, y+0);
							normals [nidx++] = new Vector3(0,1,0);
							uvs1 [uidx++] = new Vector2(u0, v1);
							triangles[tri_index+0] = vidx-3;
							triangles[tri_index+1] = vidx-2;
							triangles[tri_index+2] = vidx-1;
							tri_index += 3;
							//2nd triangle
							vertices [vidx++] = new Vector3(x+0, lay, y+py);
							normals [nidx++] = new Vector3(0,1,0);
							uvs1 [uidx++] = new Vector2(u0, v0);
							vertices [vidx++] = new Vector3(x+px, lay, y+py);
							normals [nidx++] = new Vector3(0,1,0);
							uvs1 [uidx++] = new Vector2(u1, v0);
							vertices [vidx++] = new Vector3(x+px, lay, y+0);
							normals [nidx++] = new Vector3(0,1,0);
							uvs1 [uidx++] = new Vector2(u1, v1);
							triangles[tri_index+0] = vidx-3;
							triangles[tri_index+1] = vidx-2;
							triangles[tri_index+2] = vidx-1;
							tri_index += 3;
						}
					}
					Vector3[] finalvertices = new Vector3[vidx];
					Vector3[] finalnormals = new Vector3[nidx];
					Vector2[] finaluvs1 = new Vector2[uidx];
					int[] finaltriangles = new int[tri_index];

					for (int i=0; i<vidx; i++)
						finalvertices[i] = vertices[i];
					for (int i=0; i<vidx; i++)
						finalnormals[i] = normals[i];
					for (int i=0; i<vidx; i++)
						finaluvs1[i] = uvs1[i];
					for (int i=0; i<vidx; i++)
						finaltriangles[i] = triangles[i];
					mesh.vertices = finalvertices;
					mesh.normals = finalnormals;
					mesh.uv = finaluvs1;
					mesh.SetTriangles(finaltriangles, 0);
					mesh.RecalculateNormals();
					mesh.RecalculateBounds();
				}
			}
		}
		BoxCollider co = mapobj.AddComponent<BoxCollider> ();
		co.enabled = true;
		co.size = new Vector3 (tw*m_unitSize, 1, th*m_unitSize);
		co.isTrigger = true;
		return mapobj;
	}

	Mesh CreateTilemapZone(ref GameObject layerobj, int index, ref Material mat, int ilayer)
	{
		GameObject obj = new GameObject(layerobj.name + index);
		Mesh mesh = new Mesh();
		mesh.Clear(false);
		mesh.name = layerobj.name + "_mesh" + index;
		MeshFilter mf = obj.AddComponent<MeshFilter>();
		MeshRenderer mr = obj.AddComponent<MeshRenderer>();
		mf.mesh = mesh;
		Material nmat = Instantiate (mat);
		nmat.renderQueue = ilayer;
		mr.material = nmat;
		mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
		mr.receiveShadows = true;
		obj.transform.parent = layerobj.transform;
		obj.transform.localPosition = Vector3.zero;
		return mesh;
	}
	//-------------------------------
	public void LoadComponentEdTileMap(ref GameObject node, ref string[] split, ref int k, int freeAtlasSlot)
	{
		m_freeAtlasSlot = freeAtlasSlot;

		m_levelTileList = new List<string> ();
		m_levelTileList.Clear ();
		for (;;)
		{	string str = split[k++];
			if (str.Length<1) continue;
			if (str == "TileMap") continue;
			if (str == "EndTileMap") break;
			if (str == "levelSize")
			{	str = NextString(ref split, ref k);
				Single.TryParse (str, out m_levelSize.x);
				str = split[k++];
				Single.TryParse (str, out m_levelSize.y);
				continue;
			}
			if (str == "unitSize")
			{	str = NextString(ref split, ref k);
				m_unitSize = float.Parse (str);
				continue;
			}
			if (str == "tileSize")
			{	str = NextString(ref split, ref k);
				m_tileSize = float.Parse (str);
				continue;
			}
			if (str == "tilesetSize")
			{	str = NextString(ref split, ref k);
				//int m_tilesetSize = int.Parse (str);
				int.Parse (str);
				continue;
			}
			if (str == "GroupTile")
			{
				for (;;)
				{
					str = NextString(split, ref k);
					if (str == "EndGroupTile") break;
					if (str == "WC3")
					{	str = NextString(ref split, ref k);
						m_levelTileList.Add (str);
						continue;
					}
				}
				continue;
			}
			if (str == "LevelData")
			{
				int tw=(int)m_levelSize.x, th=(int)m_levelSize.y;
				m_levelData = new byte[tw * th];
				//
				for (int i=0; i<tw*th; i++)
				{	str = NextString(split, ref k);
					ushort rlay = ushort.Parse (str);
					m_levelData [i] = (byte)rlay;
				}
				//fin du fichier en principe
			}
		}
	}
	string NextString(ref string [] split, ref int k)
	{
		string str;
		for(;;)
		{	if (k>=split.Length) return "";
			str = split[k++];
			if (str.Length>0) return str;
		}
	}

}


