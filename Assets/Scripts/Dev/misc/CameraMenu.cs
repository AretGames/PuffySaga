﻿using UnityEngine;
using System.Collections;

public class CameraMenu : MonoBehaviour {

	public void ButtonClicked(string scenename)
	{
		Messenger.Broadcast< string > (EVENT.SCENE_LOAD, scenename);
	}
}
