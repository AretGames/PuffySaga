﻿using UnityEngine;
using System.Collections;
using PuffySaga;

public class SceneTrigger : MonoBehaviour {
	public string			m_scenefrom="toto";
	public string			m_sceneto="toto";

	void OnTriggerEnter(Collider other)
	{
		Messenger.Broadcast< string > (EVENT.SCENE_LOAD, m_sceneto);
	}

}
