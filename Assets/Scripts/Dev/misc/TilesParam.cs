﻿using UnityEngine;
using System.Collections;

public class TilesParam
{
	public int					m_tilesetSize;
	public Texture2D			m_atlas;
	public Rect[]				m_rects;

}
