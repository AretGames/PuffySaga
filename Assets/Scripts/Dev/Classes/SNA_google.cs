﻿using UnityEngine;
using System.Collections;
using Soomla.Profile;

public class SNA_google : MonoBehaviour
{

	public void OnConnectGoogle()
	{
		if (!SoomlaProfile.IsLoggedIn (Provider.GOOGLE))
		{
			SoomlaProfile.Login (Provider.GOOGLE);
		}
		else
		{
			SoomlaProfile.UpdateStory(
				Provider.GOOGLE,
				"First",
				"Second",
				"Third",
				"Fourth",
				"https://play.google.com",
				""
				);
		}
	}
}
