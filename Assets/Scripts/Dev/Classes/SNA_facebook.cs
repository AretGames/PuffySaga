﻿using UnityEngine;
using System.Collections;
using Soomla.Profile;
using Soomla;

public class SNA_facebook : MonoBehaviour
{

	public void OnConnectFacebook()
	{
		if (!SoomlaProfile.IsLoggedIn (Provider.FACEBOOK))
		{
			SoomlaProfile.Login (
				Provider.FACEBOOK);//,                        // Social Provider
				//"",      									// Payload
				//new BadgeReward("loggedIn", "Logged In!") // Reward
				//);
		}
		else
		{
			SoomlaProfile.UpdateStory(
				Provider.FACEBOOK,                        // Social Provider
				"Check out this great story by SOOMLA!",   // Message
				"SOOMLA is 2 years young!",                // Name
				"SOOMLA is GROWing",                       // Caption
				"soomla_2_years",                          // Desc
				"http://blog.soom.la",                     // Link
				"http://blog.soom.la.../soombot.png",      // Image
				"",      									// Payload
				new BadgeReward("sheriff", "Sheriff")      // Reward
			);
		}
	}
}
