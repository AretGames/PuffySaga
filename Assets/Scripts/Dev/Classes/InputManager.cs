﻿using UnityEngine;
using System.Collections;
using PuffySaga;

public class InputManager {

	private bool	m_isMenuOpen;

	public InputManager()
	{
		Messenger.AddListener< float >(EVENT.UPDATE, OnUpdate );
		Messenger.AddListener< string >(EVENT.MENU_ISOPENED, OnMenuIsOpened );
		Messenger.AddListener< string >(EVENT.MENU_ISCLOSED, OnMenuIsClosed );
	}
	~InputManager()
	{
		Messenger.RemoveListener< float >(EVENT.UPDATE, OnUpdate );
		Messenger.RemoveListener< string >(EVENT.MENU_ISOPENED, OnMenuIsOpened );
		Messenger.RemoveListener< string >(EVENT.MENU_ISCLOSED, OnMenuIsClosed );
	}

	void OnMenuIsOpened(string menuname)
	{
		//Debug.Log ("menu opened:" + menuname);
		m_isMenuOpen = true;
	}

	void OnMenuIsClosed(string menuname)
	{
		//Debug.Log ("menu closed:" + menuname);
		m_isMenuOpen = false;
	}

	void OnUpdate(float elapsedTime)
	{
		if (m_isMenuOpen == true)
			return;

		// If there are two touches on the device...
		if (Input.touchCount == 2)
		{	// Store both touches.
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);
			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
			if (deltaMagnitudeDiff != 0)
				Messenger.Broadcast< float > (EVENT.MOUSE_PINCH, deltaMagnitudeDiff);
			return;
		}

		//check for clickable gameobject (ex: pnj, tilemap, etc)
		bool isdown = false;
		for (int i=0; i<3; i++)
		{	if (Input.GetMouseButton (i))
			{
				Messenger.Broadcast< int,Vector3 > (EVENT.MOUSE_BUTTON, i, Input.mousePosition);
				isdown = true;
			}
		}

		if (isdown == false)
			Messenger.Broadcast(EVENT.MOUSE_RELEASED);

	}
	
}
