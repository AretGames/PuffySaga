﻿using UnityEngine;
using System.Collections;
using PuffySaga;

public class Menu : Character
{
	void Start ()
	{
		Messenger.AddListener(EVENT.MENU_CLOSE, OnMenuClose );
		Messenger.Broadcast< string > (EVENT.MENU_ISOPENED, null);//m_obj.GetComponent<EdPnj>().m_menu);
	}

	void OnMenuClose()
	{
		Messenger.RemoveListener(EVENT.MENU_CLOSE, OnMenuClose );
		Messenger.AddListener(EVENT.MENU_ANIMATION_FINISHED, OnAnimationFinished );

		Animator anim = m_obj.GetComponent<Animator> ();
		anim.Play ("menu_disappear");
	}

	void OnAnimationFinished()
	{
		string menuname=null;
		//string menuname = new string( m_obj.GetComponent<EdPnj>().m_menu.ToCharArray() );

		Messenger.RemoveListener(EVENT.MENU_ANIMATION_FINISHED, OnAnimationFinished );
		DestroyImmediate (m_obj);

		Messenger.Broadcast< string > (EVENT.MENU_ISCLOSED, menuname);
	}
}
