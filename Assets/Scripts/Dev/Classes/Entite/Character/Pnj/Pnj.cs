﻿using UnityEngine;
using System.Collections;

namespace PuffySaga
{	
	public class Pnj : Character, Ia
	{
		void Start ()
		{
			Messenger.AddListener< int, Vector3, string >(EVENT.CLICKED, OnClicked );
			Messenger.AddListener(EVENT.SCENE_CLOSE, OnSceneClose );

		}

		void OnSceneClose ()
		{
			Messenger.RemoveListener< int, Vector3, string >(EVENT.CLICKED, OnClicked );
			Messenger.RemoveListener(EVENT.SCENE_CLOSE, OnSceneClose );

		}

		public void OnClicked(int butt, Vector3 pos, string name)
		{
			if (name.Contains (m_obj.name))
			{
				EdPnj pa = m_obj.GetComponent<EdPnj>();
				Debug.Log ("menu: " + pa.m_menu);
				Vector3 cpos = new Vector3(0,0,0);
				//
				GameObject obj = (GameObject)MonoBehaviour.Instantiate(Resources.Load("Prefabs/menu_pnj_"+pa.m_menu) as GameObject, cpos, Quaternion.identity);
				Menu m = obj.AddComponent<Menu>();
				m.m_obj = obj;
			}
		}
	}

}
