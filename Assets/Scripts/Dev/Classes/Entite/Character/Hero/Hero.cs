using UnityEngine;
using System.Collections;

namespace PuffySaga
{	
	//TEMPORAIRES A DEPLACER AILLEURS
	struct ShopItem
	{	public string name;
		public int    currency;
		public float  prix;
	};
	//fin TEMPORAIRES


	public class Hero : Character, Moving, Controllable
	{
		public float		m_softCurrency;
		public float		m_hardCurrency;
		public GUIStyle		m_textStyle;

		//==== INTERFACES ======
		Vector3				_togo = new Vector3();
		Vector3				_dir = new Vector3();
		Vector3				_velocity = new Vector3();
		float				_damping;
		float				_speed;
		public float		_maxspeed;
		public Vector3		m_togo { get { return _togo; } set { _togo = value; } }
		public Vector3		m_dir { get { return _dir; } set { _dir = value; } }
		public Vector3		m_velocity { get { return _velocity; } set { _velocity = value; } }
		public float		m_damping { get { return _damping; } set { _damping = value; } }
		public float		m_speed { get { return _speed; } set { _speed = value; } }
		public float		m_maxspeed { get { return _maxspeed; } set { _maxspeed = value; } }
		//==== fin INTERFACES ======

		//private Rigidbody	m_rb;

		void Start ()
		{
			m_softCurrency = 0;
			m_hardCurrency = 0;
			m_textStyle = new GUIStyle();

			_maxspeed = 80.0f;
			m_speed = m_maxspeed;
			m_position = new Vector3 (52.45f, 0, 23.24f);
			m_togo = m_position;
			m_obj = gameObject;
			m_rb = m_obj.GetComponent<Rigidbody> ();
			m_sprite = m_obj.transform.Find ("sprite").gameObject;
			m_animation = m_sprite.GetComponent<Animation> ();
			m_animator = m_sprite.GetComponent<Animator> ();
			m_animator.speed = 0.0f;
			
			Messenger.AddListener< float >(EVENT.UPDATE, OnUpdate );
			Messenger.AddListener< Vector3 >(EVENT.SET_POSITION, OnSetPosition );
			Messenger.AddListener< int,Vector3 >(EVENT.PRESSED, OnPressed );
			Messenger.AddListener< float >(EVENT.MENU_GIVE_COIN, OnMenuGiveCoin );
			//temporaries
			Messenger.AddListener< string >(EVENT.MENU_TRY_TO_BUY, OnMenuTryToBuy );
			Messenger.AddListener< string,int,float >(EVENT.MENU_BUY, OnMenuBuy );
		}

		public void OnDisable()
		{
			Messenger.RemoveListener< float >(EVENT.UPDATE, OnUpdate );
			Messenger.RemoveListener< Vector3 >(EVENT.SET_POSITION, OnSetPosition );
			Messenger.RemoveListener< int,Vector3 >(EVENT.PRESSED, OnPressed );
			Messenger.RemoveListener< float >(EVENT.MENU_GIVE_COIN, OnMenuGiveCoin );
			//temporaries
			Messenger.RemoveListener< string >(EVENT.MENU_TRY_TO_BUY, OnMenuTryToBuy );
			Messenger.RemoveListener< string,int,float >(EVENT.MENU_BUY, OnMenuBuy );
		}

		public void OnPressed(int butt, Vector3 pos)
		{
			pos.y = 0;
			m_togo = pos;
		}

		// Update is called once per frame
		void OnUpdate (float elapsedTime)
		{
			m_position = m_rb.position;
			float fdist = Vector3.Distance (m_position, m_togo);
			if (fdist > 0.309351f) {
				m_dir = m_togo - m_position;
				m_dir.Normalize ();
				m_rb.velocity = m_dir * m_maxspeed * elapsedTime;
			} else {
				m_rb.velocity = Vector3.zero;
			}
			if (m_animator) {
				float pspeed = m_rb.velocity.magnitude;
				if (pspeed > 0.3f) {
					pspeed = Mathf.Clamp (pspeed, 0.1f, 3.5f);
					m_animator.speed = 0.535f * pspeed;
					Vector3 dir = m_rb.velocity;
					dir.Normalize ();
					int num=0;
					//Debug.Log ("dirx:"+dir.x+", dirz:"+dir.z+", pspeed="+pspeed);
					float maxv = 0.35f;
					if (dir.x < -maxv) num += 8;
					if (dir.x > maxv) num += 2;
					if (dir.z < -maxv) num += 1;
					if (dir.z > maxv) num += 4;
					if (num>0)
						m_animator.Play ("walk"+(num/10)+""+(num%10));
					
				} else {
					m_animator.speed = 0.0f;
					//Debug.Log ("animator stopped, pspeed="+pspeed);
				}
			}
			Messenger.Broadcast< Vector3 > (EVENT.CAMERA_UPDATE_SHIFTED_TARGET, m_position);

		}

		void OnSetPosition(Vector3 npos)
		{
			m_position = npos;
			m_togo = npos;
			m_rb.position = npos;
			m_rb.velocity = Vector3.zero;
			Messenger.Broadcast< Vector3,Vector3 > (EVENT.CAMERA_INIT_SHIFTED_TARGET, m_position, new Vector3(0,20,10));
		}

		void OnMenuGiveCoin(float amount)
		{
			m_hardCurrency += amount;
		}

		//TEMPORAIRES QUI DEVRONT ETRE DEPLACES AILLEURS, DANS UN MANAGER
		//temporaries
		void OnGUI()
		{
			m_textStyle.fontSize = 40;
			m_textStyle.normal.textColor = Color.white;
			GUI.Label(new Rect( 0,Screen.height-40, 70,30),"$"+m_hardCurrency+"  #"+m_softCurrency,m_textStyle);
		}

		//temporaries
		void OnMenuTryToBuy(string szitem)
		{
			//initialisés ici, mais en fait devront etre chargés d'un fichier
			const int nbitems=2;
			ShopItem[] list_des_items = new ShopItem[nbitems];
			list_des_items[0].name = "bonbon";
			list_des_items[0].currency = 1;
			list_des_items[0].prix = 30;
			list_des_items[1].name = "sucrerie";
			list_des_items[1].currency = 1;
			list_des_items[1].prix = 210;
			//
			for (int i=0; i<nbitems; i++)
			{
				if (szitem != list_des_items[i].name) continue;
				//on regarde si le joueur peut acheter le truc
				bool isok = false;
				if (list_des_items[i].currency==0)
				{	if (list_des_items[i].prix < m_softCurrency) isok=true;
				}
				else
				{	if (list_des_items[i].prix < m_hardCurrency) isok=true;
				}
				//
				if (isok==false)
				{	//afficher le menu "NOT ENOUGH MONEY"
				}
				else
				{	
					Messenger.Broadcast< string,int,float > (EVENT.MENU_OK_TO_BUY, szitem, list_des_items[i].currency, list_des_items[i].prix);
				}
			}
		}

		void OnMenuBuy(string szitem, int currency, float fprix)
		{
			if (currency == 0)
			{
				m_softCurrency -= fprix;
			}
			else
			{
				m_hardCurrency -= fprix;
			}
			//faire un gestionnaire d'articles achetés
			if (szitem == "bonbon")
				m_softCurrency += 10;
			if (szitem == "sucrerie")
				m_softCurrency += 100;
		}
	}
}