﻿using UnityEngine;
using System.Collections;

namespace PuffySaga
{	
	public class Character : Entite
	{
		public Animation			m_animation;// {get; set;}
		public Animator				m_animator;// {get; set;}
		public GameObject			m_sprite;// {get; set;}
		public _stats				m_stats;
		public Rigidbody			m_rb;
		public CharacterController	m_CharacterController;

		public void Reset()
		{	m_stats = new _stats();
			m_stats.current = new Stats();
			m_stats.initial = new Stats();
			m_stats.current.Reset ();
			m_stats.initial.Reset ();
		}
	}
}
