﻿using UnityEngine;
using System.Collections;
using PuffySaga;

public class Cameraman : Character, Moving
{
	public Vector3		m_shift;
	Camera				m_camera;
	public float		m_zoomSpeed = 0.5f;        // The rate of change of the zoom
	private bool		m_initialized = false;

	//==== INTERFACES ======
	Vector3				_togo = new Vector3();
	Vector3				_dir = new Vector3();
	Vector3				_velocity = new Vector3();
	float				_damping;
	float				_speed;
	public float		_maxspeed;
	public Vector3		m_togo { get { return _togo; } set { _togo = value; } }
	public Vector3		m_dir { get { return _dir; } set { _dir = value; } }
	public Vector3		m_velocity { get { return _velocity; } set { _velocity = value; } }
	public float		m_damping { get { return _damping; } set { _damping = value; } }
	public float		m_speed { get { return _speed; } set { _speed = value; } }
	public float		m_maxspeed { get { return _maxspeed; } set { _maxspeed = value; } }
	//==== fin INTERFACES ======

	private bool		m_mustReleaseMouse;
	// Use this for initialization
	void Start ()
	{
		if (m_initialized == true)
			return;
		Init ();
	}

	public void Init()
	{	m_initialized = true;
		m_obj = gameObject;
		m_shift = Vector3.zero;
		m_position = Vector3.zero;
		m_camera = m_obj.GetComponent<Camera> ();
		m_camera.orthographicSize = 5.0f;
		m_maxspeed = Mathf.Max (m_maxspeed, 4.0f);

		m_mustReleaseMouse = false;

		Messenger.AddListener< float >(EVENT.UPDATE, OnUpdate );
		Messenger.AddListener(EVENT.CAMERA_GET, OnCameraGet );
		Messenger.AddListener< Vector3,Vector3 >(EVENT.CAMERA_INIT_SHIFTED_TARGET, OnCameraInitShiftedTarget );
		Messenger.AddListener< Vector3 >(EVENT.CAMERA_UPDATE_SHIFTED_TARGET, OnCameraUpdateShiftedTarget );
		Messenger.AddListener< int,Vector3 >(EVENT.MOUSE_BUTTON, OnMouseButton );
		Messenger.AddListener< float >(EVENT.MOUSE_PINCH, OnMousePinch );
		Messenger.AddListener(EVENT.MOUSE_RELEASED, OnMouseReleased );
	}

	public void OnDisable()
	{
		Messenger.RemoveListener< float >(EVENT.UPDATE, OnUpdate );
		Messenger.RemoveListener(EVENT.CAMERA_GET, OnCameraGet );
		Messenger.RemoveListener< Vector3,Vector3 >(EVENT.CAMERA_INIT_SHIFTED_TARGET, OnCameraInitShiftedTarget );
		Messenger.RemoveListener< Vector3 >(EVENT.CAMERA_UPDATE_SHIFTED_TARGET, OnCameraUpdateShiftedTarget );
		Messenger.RemoveListener< int,Vector3 >(EVENT.MOUSE_BUTTON, OnMouseButton );
		Messenger.RemoveListener< float >(EVENT.MOUSE_PINCH, OnMousePinch );
		Messenger.RemoveListener(EVENT.MOUSE_RELEASED, OnMouseReleased );
	}

	void OnUpdate(float elapsedTime)
	{
		if (gameObject == null)
			return;

		float fdist = Vector3.Distance (m_position, m_togo);
		float fspeed = 0;
		m_dir = m_togo - m_position;
		m_dir.Normalize ();
		fspeed = Mathf.Clamp (m_maxspeed * fdist, 0.0f, m_maxspeed);
		m_velocity = m_dir * fspeed * elapsedTime;
		m_position += m_velocity;
		
		m_obj.transform.position = m_position;
		m_obj.transform.rotation = m_rotation;
	}

	void OnCameraInitShiftedTarget(Vector3 position, Vector3 shift)
	{
		m_position = position + shift;
		m_shift = shift;
		m_togo = m_position;
		//gameObject.transform.LookAt (position);
		m_rotation = Quaternion.Euler (new Vector3(70,0,0));
	}

	void OnCameraUpdateShiftedTarget(Vector3 position)
	{
		m_togo = position + m_shift;
	}

	void OnCameraGet()
	{
		Camera cam = null;
		#if UNITY_EDITOR
			if (Application.isPlaying)
			{	if (gameObject == null) return;
				cam = m_obj.GetComponent<Camera> ();
			}
			else
			{	if (Camera.current == null)
					return;
				cam = Camera.current.GetComponent<Camera> ();
			}
		#else // UNITY_EDITOR
			if (gameObject == null) return;
			cam = m_obj.GetComponent<Camera> ();
		#endif // UNITY_EDITOR
		Messenger.Broadcast< Camera > (EVENT.CAMERA_RECEIVED, cam);
	}

	void OnMouseButton(int butt, Vector3 mousepos)
	{
		Ray ray = m_camera.ScreenPointToRay (mousepos);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit))
		{
			if (hit.transform.gameObject.name=="levelTileMap")
			{	Messenger.Broadcast< int, Vector3 > (EVENT.PRESSED, butt, hit.point);
				m_mustReleaseMouse = true;
			}
			else
			{	if (m_mustReleaseMouse == true)
					return;
				Messenger.Broadcast< int, Vector3, string > (EVENT.CLICKED, butt, hit.point, hit.transform.gameObject.name);
				m_mustReleaseMouse = true;
			}
		}
	}

	void OnMouseReleased()
	{
		m_mustReleaseMouse = false;
	}

	void OnMousePinch(float deltaMagnitudeDiff)
	{	// If the camera is orthographic...
		if (m_camera.orthographic == true)
		{	// ... change the orthographic size based on the change in distance between the touches.
			m_camera.orthographicSize += deltaMagnitudeDiff * m_zoomSpeed;
			// Make sure the orthographic size never drops below zero.
			m_camera.orthographicSize = Mathf.Clamp(m_camera.orthographicSize, 2.0f, 16.0f);
		}
		else
		{	// Otherwise change the field of view based on the change in distance between the touches.
			m_camera.fieldOfView += deltaMagnitudeDiff * m_zoomSpeed;
			// Clamp the field of view to make sure it's between 0 and 180.
			m_camera.fieldOfView = Mathf.Clamp(m_camera.fieldOfView, 0.1f, 179.9f);
		}
	}
}
