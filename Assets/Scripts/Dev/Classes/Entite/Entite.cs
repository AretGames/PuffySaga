﻿using UnityEngine;
using System.Collections;

namespace PuffySaga
{	
	public class Entite : MonoBehaviour
	{
		public Vector3		m_position;// {get; set;}
		public GameObject	m_obj;// { get; set; }
		public string		m_name;
		public Quaternion	m_rotation;
		public Vector3		m_scale;
	}
}