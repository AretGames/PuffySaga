﻿using UnityEngine;
using System.Collections;
using PuffySaga;

public class Scene_Pnj
{
	public Scene_Pnj(GameObject[] pnjobjs, string scenefrom)
	{
		//get all pnj templates and load corresponding prefabs
		foreach (var pobj in pnjobjs)
		{	EdPnj param = pobj.GetComponent<EdPnj>();
			GameObject obj = (GameObject)MonoBehaviour.Instantiate(Resources.Load("Prefabs/pnj_"+param.m_name) as GameObject, pobj.transform.position, pobj.transform.rotation);
			Pnj p = obj.AddComponent<Pnj>();
			p.m_obj = obj;
			EdPnj pa = obj.AddComponent<EdPnj>();
			pa.m_name = param.m_name;
			pa.m_menu = param.m_menu;
		}

		//after finished, destroy pnj templates, they are no more needed
		foreach (var pnjobj in pnjobjs)
		{	MonoBehaviour.DestroyImmediate(pnjobj);
		}
	}

	public void SceneClose()
	{
	}

}
