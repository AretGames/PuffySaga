﻿using UnityEngine;
using System.Collections;

public class Scene_Hero
{
	private Vector3				m_playerPos;
	private AsyncOperation		m_async;		//handle for asynchronous operation
	private FunctionPointer		m_fPointer;		//pointer to a function to call after the level has been loaded

	public Scene_Hero(GameObject[] portalobjs, string scenefrom)
	{
		m_async = null;

		//don't destroy portals, they are used to jump from levels to levels
		Vector3 player_pos = Vector3.zero;
		foreach (var portalobj in portalobjs)
		{
			SceneTrigger scnt = portalobj.GetComponent<SceneTrigger>();
			if ((scnt.m_sceneto == scenefrom) || (scenefrom=="selectArea") || (scenefrom==""))
			{	GameObject respawn = portalobj.transform.Find ("respawn").gameObject;
				player_pos = respawn.transform.position;
				break;
			}
		}
		//if a portal has been found, then add the scene containing the player
		if (player_pos != Vector3.zero) // && m_sceneto != "logo") {
		{	m_playerPos = player_pos;
			DoLoadLevelAdditiveAsync("player", new FunctionPointer(RepositionPlayer));
		}

		Messenger.AddListener< float >(EVENT.UPDATE, OnUpdate );
		
	}
	public void SceneClose()
	{
		Messenger.RemoveListener< float >(EVENT.UPDATE, OnUpdate );
	}

	void OnUpdate(float elapsedTime)
	{
		if (m_async != null && m_async.isDone)
		{
			m_async = null;
			//Scheduler.RegisterEvent(m_timer, m_fPointer);
			m_fPointer();
		}
	}
	

	void RepositionPlayer()
	{
		Messenger.Broadcast< Vector3 > (EVENT.SET_POSITION, m_playerPos);
	}
	
	void DoLoadLevelAdditiveAsync(string levelname, FunctionPointer fPointer)
	{
		m_fPointer = fPointer;
		m_async = Application.LoadLevelAdditiveAsync(levelname);
	}
}
