﻿using UnityEngine;
using System.Collections;
using PuffySaga;

public class Scene_Player
{
	public Scene_Player(GameObject[] portalobjs, GameObject[] spawnobjs, string scenefrom)
	{
		if (spawnobjs.Length > 0)
		{	//get all spawn templates and load corresponding prefabs
			foreach (var pobj in spawnobjs)
			{	GameManager.m_spawnPos = pobj.transform.position;
			}
			//after finished, destroy player templates, they are no more needed
			foreach (var pobj in spawnobjs)
			{	MonoBehaviour.DestroyImmediate(pobj);
			}
		}
		//---------
		if (portalobjs.Length > 0)
		{
			//don't destroy portals, they are used to jump from levels to levels
			Vector3 player_pos = Vector3.zero;
			foreach (var portalobj in portalobjs)
			{
				SceneTrigger scnt = portalobj.GetComponent<SceneTrigger>();
				if ((scnt.m_sceneto == scenefrom) || (scenefrom=="selectArea") || (scenefrom==""))
				{	GameObject respawn = portalobj.transform.Find ("respawn").gameObject;
					player_pos = respawn.transform.position;
					break;
				}
			}
			//if a portal has been found, then add the scene containing the player
			if (player_pos != Vector3.zero) // && m_sceneto != "logo") {
			{	//m_playerPos = player_pos;
				//DoLoadLevelAdditiveAsync("player", new FunctionPointer(RepositionPlayer));
			}
		}
	}

	public void SceneClose()
	{
	}

	public GameObject SpawnPlayer(string playername, Vector3 pos)
	{	string pname = GameManager.m_playerParams.type;
		string pfilename = GameManager.m_playerPath+"/"+pname;
		pos.y = 1;
		GameObject obj = (GameObject)MonoBehaviour.Instantiate(Resources.Load(pfilename) as GameObject, pos, Quaternion.identity);
		Puffy pf = obj.AddComponent<Puffy>();
		pf.Reset ();
		pf.m_position = pos;
		pf.m_rotation = Quaternion.identity;
		//si c'est le joueur local on positionne la camera
		pf.PositionneCamera();
		//on le parentise au gameobject PLAYERS pour faciliter les recherches
		obj.transform.parent = GameManager.m_gobjPlayers.transform;
		return obj;
	}
	public GameObject SpawnEnemy(string playername, Vector3 pos)
	{	string pname = GameManager.m_playerParams.type;
		string pfilename = GameManager.m_playerPath+"/"+pname;
		pos.y = 1;
		GameObject obj = (GameObject)MonoBehaviour.Instantiate(Resources.Load(pfilename) as GameObject, pos, Quaternion.identity);
		//on le parentise au gameobject PLAYERS pour faciliter les recherches
		GameObject.Destroy (obj.GetComponent<Rigidbody>());
		obj.transform.parent = GameManager.m_gobjPlayers.transform;
		return obj;
	}
}
