﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class Scene
{

	public string				m_sceneto="";
	private string				m_scenefrom="";
	private const string		TAG_PORTAL = "TAG_PORTAL";   //http://answers.unity3d.com/questions/33597/is-it-possible-to-create-a-tag-programmatically.html
	private const string		TAG_PNJ = "TAG_PNJ";   //http://answers.unity3d.com/questions/33597/is-it-possible-to-create-a-tag-programmatically.html
	private const string		TAG_SPAWN = "TAG_SPAWN";   //http://answers.unity3d.com/questions/33597/is-it-possible-to-create-a-tag-programmatically.html

	public Scene_Player			m_scenePlayer;
	//public Scene_Monster		m_sceneMonster;
	public Scene_Pnj			m_scenePnj;



	public Scene(string sceneto, string scenefrom)
	{
		m_sceneto = sceneto;
		m_scenefrom = scenefrom;

		Messenger.AddListener<int>(EVENT.SCENE_LOADED, OnSceneLoaded );
		Messenger.AddListener(EVENT.SCENE_CLOSE, OnSceneClose );

		Application.LoadLevel(m_sceneto);

	}

	void OnSceneClose()
	{
		if (m_scenePlayer != null)
			m_scenePlayer.SceneClose ();
		if (m_scenePnj != null)
			m_scenePnj.SceneClose ();
		Messenger.RemoveListener<int>(EVENT.SCENE_LOADED, OnSceneLoaded );
		Messenger.RemoveListener(EVENT.SCENE_CLOSE, OnSceneClose );
		Messenger.Broadcast(EVENT.SCENE_DELETED);
	}

	void OnSceneLoaded(int level)
	{
		//
		if (m_sceneto.Contains ("level"))
		{	GameManager.m_isPlayableLevel = true;
			//si c'est un level on supprime toutes les cameras et on crée la camera suiveuse
			GameObject[] camobjs = GameObject.FindGameObjectsWithTag("MainCamera");
			foreach (var cobj in camobjs)
			{	MonoBehaviour.DestroyImmediate(cobj);
			}
			//
			GameObject camobj = new GameObject("Cameraman");
			Camera cam = camobj.AddComponent<Camera>();
			Cameraman cman = camobj.AddComponent<Cameraman>();		//on lui rajoute le script de gestion de la camera
			cman.Init();
			//
			//if scene have portals or pnj, then it is a playing level
			GameObject[] spawnobjs = GameObject.FindGameObjectsWithTag(TAG_SPAWN);
			GameObject[] portalobjs = GameObject.FindGameObjectsWithTag(TAG_PORTAL);
			GameObject[] pnjobjs = GameObject.FindGameObjectsWithTag(TAG_PNJ);
			if (portalobjs.Length > 0 || spawnobjs.Length > 0)
				m_scenePlayer = new Scene_Player(portalobjs, spawnobjs, m_scenefrom);
			if (pnjobjs.Length > 0)
				m_scenePnj = new Scene_Pnj(pnjobjs, m_scenefrom);
		}
		else
		{	GameManager.m_isPlayableLevel = false;
		}
	}

}

