﻿using UnityEngine;

public class EVENT
{
	static public string CAMERA_GET						="OnCameraGet";
	static public string CAMERA_RECEIVED				="OnCameraReceived";
	static public string CAMERA_INIT_SHIFTED_TARGET		="OnCameraInitShiftedTarget";
	static public string CAMERA_UPDATE_SHIFTED_TARGET	="OnCameraUpdateShiftedTarget";
	static public string SCENE_LOAD						="OnLoadScene";
	static public string SCENE_LOADED					="OnSceneLoaded";
	static public string SCENE_CLOSE					="OnSceneClose";
	static public string SCENE_DELETED					="OnSceneDeleted";
	//static public string SCENE_SPAWN_PLAYER				="OnSceneSpawnPlayer";

	static public string SET_POSITION					="OnSetPosition";
	static public string CLICKED						="OnClicked";
	static public string PRESSED						="OnPressed";
	static public string MOUSE_BUTTON					="OnMouseButton";
	static public string MOUSE_PINCH					="OnMousePinch";
	static public string MOUSE_RELEASED					="OnMouseReleased";
	static public string UPDATE							="OnUpdate";

	static public string MENU_CLOSE						="OnMenuClose";
	static public string MENU_ANIMATION_FINISHED		="OnMenuAnimationFinished";
	static public string MENU_ISOPENED					="OnMenuIsOpened";
	static public string MENU_ISCLOSED					="OnMenuIsClosed";
	static public string MENU_BACK						="OnMenuBack";

	static public string MENU_GIVE_COIN					="OnMenuGiveCoin";

	static public string MENU_TRY_TO_BUY				="OnMenuTryToBuy";
	static public string MENU_OK_TO_BUY					="OnMenuOkToBuy";
	static public string MENU_BUY						="OnMenuBuy";

	static public string MENUFADE_INIT					="OnMenuFadeInit";
	static public string MENUFADE_GET_TEXB				="OnMenuFadeGetTextureB";

}
