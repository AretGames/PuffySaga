﻿using UnityEngine;
using System.Collections;
using PuffySaga;

public class SceneManager {

	public Scene		m_scene = null;
	public string		m_prevScene="";		//last scene that was loaded (or from wich we are comming)
	public string		m_nextScene="";		//scene to load at the next update
	string				m_warmNextScene="";
	public SceneManager()
	{	
		Messenger.AddListener< string >(EVENT.SCENE_LOAD, OnLoadScene );
		Messenger.AddListener< float >( EVENT.UPDATE, OnUpdate );
		Messenger.AddListener(EVENT.SCENE_DELETED, OnSceneDeleted );
		//Messenger.AddListener<string>(EVENT.SCENE_SPAWN_PLAYER, OnSpawnPlayer );
	}
	~SceneManager()
	{
		Messenger.RemoveListener< string >(EVENT.SCENE_LOAD, OnLoadScene );
		Messenger.RemoveListener< float >(EVENT.UPDATE, OnUpdate );
		Messenger.RemoveListener(EVENT.SCENE_DELETED, OnSceneDeleted );
		//Messenger.RemoveListener<string>(EVENT.SCENE_SPAWN_PLAYER, OnSpawnPlayer );
	}

	void OnUpdate(float elapsedTime)
	{
		if (CheckNewScene () == true)
			return;
	}

	bool CheckNewScene()
	{	if (m_nextScene.Length > 0)
		{
			GameManager.m_menuFade.SetMenuFadeRenderTarget(true);

			m_scene = new Scene (m_nextScene, m_prevScene);
			m_prevScene = m_nextScene;
			m_nextScene = "";
			return true;
		}
		return false;
	}

	void OnLoadScene(string scenename)
	{
		GameManager.m_networkIsReady = false;

		m_warmNextScene = scenename;
		if (m_scene != null)
			Messenger.Broadcast(EVENT.SCENE_CLOSE);
		else
			Messenger.Broadcast(EVENT.SCENE_DELETED);
	}

	void OnSceneDeleted()
	{	//Application.LoadLevel("void");
		m_scene = null;
		m_nextScene = m_warmNextScene;
	}

	GameObject SpawnPlayer(string pname, Vector3 pos)
	{
		if (m_scene != null && m_scene.m_scenePlayer!=null)
			return m_scene.m_scenePlayer.SpawnPlayer(pname, pos);
		return null;
	}
	GameObject SpawnEnemy(string pname, Vector3 pos)
	{
		if (m_scene != null && m_scene.m_scenePlayer!=null)
			return m_scene.m_scenePlayer.SpawnEnemy(pname, pos);
		return null;
	}
}
