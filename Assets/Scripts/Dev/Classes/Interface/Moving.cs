﻿using System;
using UnityEngine;

namespace PuffySaga
{
	public interface Moving
	{
		Vector3			m_togo { get; set; }
		Vector3			m_dir { get; set; }
		Vector3			m_velocity { get; set; }
		float			m_damping { get; set; }
		float			m_speed { get; set; }
		float			m_maxspeed { get; set; }
	}
}
