﻿using System;
using UnityEngine;

namespace PuffySaga
{
	public interface Controllable
	{
		void OnPressed(int button, Vector3 pos);
	}
}
