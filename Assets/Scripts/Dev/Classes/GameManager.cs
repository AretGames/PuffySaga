﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PuffySaga;
using Soomla.Profile;


//----------------------------------------
// NETWORKING
//----------------------------------------

//----------------------------------------
// fin NETWORKING
//----------------------------------------

public class GameManager : MonoBehaviour
{
	public enum pfNetworkStat
	{	busy,
		notConnected,
		connected
	};
	public string				m_startScene="";	//scene to load at the next update
	static public List<string>	m_stackScene = new List<string> ();

	static public SceneManager	m_sceneManager;		//handle for the Scene Manager
	static public InputManager	m_inputManager;		//handle for the Input Manager
	static public GameObject	m_obj;				//gameobject associated to the Game Manager
	static public string		m_prefabPath="PlaceHolder/Prefabs";
	static public string		m_playerPath="PlaceHolder/Characters";
	static public string		m_levelPath="PlaceHolder/Levels";
	static public string		m_edscenePath="PlaceHolder/Scenes";
	static public string		m_texturePath="PlaceHolder";

	static public GameObject	m_gobjPlayers;
	static public GameObject	m_gobjMonsters;
	static public GameObject	m_gobjItems;
	static public MenuFade		m_menuFade;
	static public GameObject	m_cancel;
	static public GameObject	m_gameController;
	static public bool			m_isPlayableLevel;

	[Range(0.0F, 1.0F)]
	public float				m_menuFadeFactor=0.0f;

	public struct PlayerParam
	{	public string	type;
		public string	name;
	};
	public struct LevelParam
	{	public string	name;
	};
	
	static public PlayerParam	m_playerParams = new PlayerParam();
	static public LevelParam	m_levelParam = new LevelParam();
	static public Vector3		m_spawnPos = new Vector3();
	//----------------------------------------
	// NETWORKING
	//----------------------------------------
	//static public string apiKey="e84faded1033d8cfb3478fb377ea937524d883eb992aa41f76a322c823fc242f";
	//static public string secretKey="3754f0a9d33fda57a80c77046e1f6e68e46ed071087747d37d87643523f1f966";
	//static public string roomid="662049829";
	//static Listener listen;
	static public string		m_username;
	static public bool			m_networkIsReady;
	static public pfNetworkStat	m_networkStat;
	//public float 				m_networkInterval = 0.1f;
	//----------------------------------------
	// fin NETWORKING
	//----------------------------------------
	//----------------------------------------
	// Social Network Account
	//----------------------------------------

	//----------------------------------------
	// fin Social Network Account
	//----------------------------------------


	// initialization
	void Start ()
	{
		m_obj = gameObject;
		DontDestroyOnLoad (m_obj);							//the GameManager will be alive for the lifetime of the game

		//----------------------------------------
		// Social Network Account
		//----------------------------------------
		// examples of catching fired events
		ProfileEvents.OnSoomlaProfileInitialized += () =>
		{
			//isInit = true;
			print ("Soomla init");
		};
		
		ProfileEvents.OnLoginFinished += (UserProfile UserProfile, string payload) =>
		{
			//SoomlaProfile.GetContacts(Provider.TWITTER);
			SoomlaProfile.GetContacts(Provider.GOOGLE);
			print ("Login Finished");
		};
		
		ProfileEvents.OnLoginFailed += (Provider Provider, string str1, string str2) =>
		{
			print ("Login failed");	
		};
		ProfileEvents.OnLoginCancelled += (Provider Provider, string str1) =>
		{
			print ("Login cancelled");			
		};
		ProfileEvents.OnSocialActionFailed += (Provider Provider, SocialActionType actionType, string str1, string str2) =>
		{
			print ("Social Action Failed");
		};
		ProfileEvents.OnSocialActionFinished += (Provider Provider, SocialActionType actionType, string str1) =>
		{
			print ("Social Action Finished");
		};
		//ProfileEvents.OnGetContactsFinished += (Provider provider, List<UserProfile> profiles, string payload) =>
		//{
		//	print ("Get Contacts Finished");
		//};
		SoomlaProfile.Initialize ();
		#if UNITY_IPHONE
		Handheld.SetActivityIndicatorStyle(iOSActivityIndicatorStyle.Gray);
		#elif UNITY_ANDROID
		Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
		#endif		
		//----------------------------------------
		// fin Social Network Account
		//----------------------------------------

		//p2p.AllocateArea ();
		// create a unique name (current time stamp)
		m_username = System.DateTime.UtcNow.Ticks.ToString();

		m_sceneManager = new SceneManager();				//create the SceneManager
		m_inputManager = new InputManager ();				//create the Input Manager
		m_menuFade = new MenuFade();

		//initialisations à faire dans le menu de selection
		m_playerParams.type = "Puffy";
		m_playerParams.name = "Janto93";
		m_levelParam.name = "level00";
		//----------------------------------------

		//keep constant event on memory (or they will be destroyed each time a level is loaded)
		Messenger.MarkAsPermanent (EVENT.SCENE_LOAD);
		Messenger.MarkAsPermanent (EVENT.SCENE_LOADED);
		Messenger.MarkAsPermanent (EVENT.SCENE_CLOSE);
		Messenger.MarkAsPermanent (EVENT.SCENE_DELETED);
		//Messenger.MarkAsPermanent (EVENT.SCENE_SPAWN_PLAYER);
		Messenger.MarkAsPermanent (EVENT.UPDATE);
		Messenger.MarkAsPermanent (EVENT.SET_POSITION);
		Messenger.MarkAsPermanent (EVENT.CAMERA_GET);
		Messenger.MarkAsPermanent (EVENT.CAMERA_RECEIVED);
		Messenger.MarkAsPermanent (EVENT.CAMERA_INIT_SHIFTED_TARGET);
		Messenger.MarkAsPermanent (EVENT.CAMERA_UPDATE_SHIFTED_TARGET);
		Messenger.MarkAsPermanent (EVENT.CLICKED);
		Messenger.MarkAsPermanent (EVENT.PRESSED);
		Messenger.MarkAsPermanent (EVENT.MOUSE_BUTTON);
		Messenger.MarkAsPermanent (EVENT.MOUSE_PINCH);
		Messenger.MarkAsPermanent (EVENT.MOUSE_RELEASED);
		Messenger.MarkAsPermanent (EVENT.MENU_ISOPENED);
		Messenger.MarkAsPermanent (EVENT.MENU_ISCLOSED);
		Messenger.MarkAsPermanent (EVENT.MENU_BACK);

		Messenger.MarkAsPermanent (EVENT.MENU_BUY);

		Messenger.MarkAsPermanent (EVENT.MENUFADE_INIT);
		Messenger.MarkAsPermanent (EVENT.MENUFADE_GET_TEXB);

		Messenger.AddListener<bool>(EVENT.MENUFADE_INIT, OnMenuFadeInit );
		Messenger.AddListener(EVENT.MENUFADE_GET_TEXB, OnMenuFadeGetTextureB );
		Messenger.AddListener(EVENT.MENU_BACK, OnMenuBack );
		Messenger.AddListener< string >(EVENT.SCENE_LOAD, OnSceneLoad );

		Messenger.Broadcast< string > (EVENT.SCENE_LOAD, m_startScene);	//load starting scene
		m_networkStat = pfNetworkStat.busy;
		//m_networkStat = pfNetworkStat.notConnected;	//debug
	}

	//----------------------------------------
	// NETWORKING
	//----------------------------------------
	void OnPreRender()
	{
		m_menuFade.SetMenuFadeRenderTarget();
	}
	void OnPostRender()
	{
		m_menuFade.SetMenuFadeRenderTarget();
	}
	void OnGUI()
	{
		//m_menuFade.SetMenuFadeRenderTarget();
		//GUI.contentColor = Color.black;
		//GUI.Label(new Rect(10,10,500,200), listen.getDebug());
		m_menuFade.OnGUI();
	}

	void OnApplicationQuit()
	{	
		//p2p.FreeArea();
	}

	static public GameObject SpawnPlayer(string pname, Vector3 pos)
	{
		return m_sceneManager.m_scene.m_scenePlayer.SpawnPlayer(pname, pos);
	}
	static public GameObject SpawnEnemy(string pname, Vector3 pos)
	{
		return m_sceneManager.m_scene.m_scenePlayer.SpawnEnemy(pname, pos);
	}

	//----------------------------------------
	// fin NETWORKING
	//----------------------------------------

	// Update is called once per frame
	void Update ()
	{	if (m_menuFade.m_isFading==true && m_menuFade.m_isFadeStarted==true)
		{	m_menuFadeFactor += Time.deltaTime*3.001f;//3.0f;
			if (m_menuFadeFactor >= 1.0f)
				m_menuFadeFactor = 1.0f;
		}
		m_menuFade.m_fadeFactor = m_menuFadeFactor;
		m_menuFade.Update(Time.deltaTime);
		Messenger.Broadcast< float > (EVENT.UPDATE, Time.deltaTime);
		//if (m_networkIsReady)
		//	WarpClient.GetInstance().Update();
	}
	void OnSceneLoad(string sname)
	{
		m_stackScene.Add (sname);
	}
	void OnLevelWasLoaded(int level)
	{
		Messenger.Broadcast< int > (EVENT.SCENE_LOADED, level);
		if (m_gobjPlayers != null)
			DestroyImmediate (m_gobjPlayers);
		if (m_gameController!=null)
			DestroyImmediate (m_gameController);

		if (GameManager.m_isPlayableLevel == true)
		{	m_gobjPlayers = new GameObject("Players");

			m_gameController = new GameObject("GameController");
			sfsSoundManager soundmanager = m_gameController.AddComponent<sfsSoundManager>();
			sfsPlayerManager playermanager = m_gameController.AddComponent<sfsPlayerManager>();
			sfsGameHUD gamehud = m_gameController.AddComponent<sfsGameHUD>();
			sfsNetworkManager networkmanager = m_gameController.AddComponent<sfsNetworkManager>();
			sfsTimeManager timemanager = m_gameController.AddComponent<sfsTimeManager>();
		}
		//m_networkIsReady = true;
		m_menuFade.SceneLoaded(level);
	}
	public void OnMenuFadeInit(bool sens)
	{	m_menuFadeFactor = 0;
		m_menuFade.m_fadeFactor = m_menuFadeFactor;
		m_menuFade.Init (sens);
		StartCoroutine(m_menuFade.RenderSceneToTexture(m_menuFade.m_texa));
	}
	public void OnMenuFadeGetTextureB()
	{	StartCoroutine(m_menuFade.RenderSceneToTexture(m_menuFade.m_texb));
	}
	public void OnMenuBack()
	{	m_menuFadeFactor = 0;
		m_menuFade.m_fadeFactor = m_menuFadeFactor;
		OnMenuFadeInit(false);
		if (m_stackScene.Count>=2)
		{	m_stackScene.RemoveAt(m_stackScene.Count-1);
			string lastscene = m_stackScene[m_stackScene.Count-1];
			m_stackScene.RemoveAt(m_stackScene.Count-1);
			Messenger.Broadcast< string > (EVENT.SCENE_LOAD, lastscene);
		}
	}

	//----------------------------------------
	// NETWORKING
	//----------------------------------------

	//----------------------------------------
	// fin NETWORKING
	//----------------------------------------
}
