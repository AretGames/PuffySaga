﻿using UnityEngine;
using System.Collections;
using Soomla.Profile;
using Soomla;

public class SNA_twitter : MonoBehaviour
{

	public void OnConnectTwitter()
	{
		if (!SoomlaProfile.IsLoggedIn (Provider.TWITTER))
		{
			SoomlaProfile.Login (Provider.TWITTER);
		}
		else
		{
			SoomlaProfile.UpdateStory(
				Provider.TWITTER,
				"Check out this great story by SOOMLA!",   // Message
				"SOOMLA is 2 years young!",                // Name
				"SOOMLA is GROWing",                       // Caption
				"soomla_2_years",                          // Desc
				"http://blog.soom.la",                     // Link
				"http://blog.soom.la.../soombot.png",      // Image
				"",      									// Payload
				new BadgeReward("sheriff", "Sheriff")      // Reward
				);
		}
	}
}
