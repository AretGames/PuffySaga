﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PuffySaga;

//----------------------------------------
// NETWORKING
//----------------------------------------

//----------------------------------------
// fin NETWORKING
//----------------------------------------

public class Puffy : Character, Moving, Controllable
{
	//==== INTERFACES ======
	Vector3				_togo = new Vector3();
	Vector3				_dir = new Vector3();
	Vector3				_velocity = new Vector3();
	float				_damping;
	float				_speed;
	public float		_maxspeed;
	public Vector3		m_togo { get { return _togo; } set { _togo = value; } }
	public Vector3		m_dir { get { return _dir; } set { _dir = value; } }
	public Vector3		m_velocity { get { return _velocity; } set { _velocity = value; } }
	public float		m_damping { get { return _damping; } set { _damping = value; } }
	public float		m_speed { get { return _speed; } set { _speed = value; } }
	public float		m_maxspeed { get { return _maxspeed; } set { _maxspeed = value; } }

	public void OnPressed(int butt, Vector3 pos)
	{
		//if( m_photonView.isMine == true )
		{	pos.y = 1;
			m_togo = pos;
		}
	}
	//==== FIN INTERFACES ======

	//==== NETWORKING ======
	public int			m_playerID;
	//==== FIN NETWORKING ======

	public struct ExtraStats
	{	public float chance;	//probabilité de trouver des bonus plutot que des malus / de moins rater les coups
		public float sagesse;	//durée des items magiques / resistance des equipements
		public void Reset()
		{	chance = 0;
			sagesse = 0;
		}
	}
	public struct _extra_stats
	{	public ExtraStats	current;		//characteristiques actuels du personnage
		public ExtraStats	initial;		//characteristiques de base du personnage
	}

	public _extra_stats		m_estats;		//stats supplementaire pour les joueurs
	public List<BSlot>		m_bag;			//sac du joueur
	public List<ESlot>		m_equipment;	//equipements du jouer

	new public void Reset()
	{	base.Reset();
		m_estats = new _extra_stats();
		m_estats.current.Reset ();
		m_estats.initial.Reset ();
		m_bag = new List<BSlot>();
		m_bag.Clear ();
		m_equipment = new List<ESlot>();
		m_equipment.Clear ();
	}

	public void PositionneCamera()
	{	
		Messenger.Broadcast< Vector3,Vector3 > (EVENT.CAMERA_INIT_SHIFTED_TARGET, m_position, new Vector3 (0, 18.6f, -7.8f));
	}
	//------------------------------
	//------------------------------
	void Start ()
	{
		_maxspeed = 80.0f;
		m_speed = m_maxspeed;
		m_velocity = Vector3.zero;
		m_damping = 0.85f;
		//m_position = new Vector3 (52.45f, 0, 23.24f);
		m_togo = m_position;
		m_obj = gameObject;
		//m_CharacterController = m_obj.GetComponent<CharacterController> ();

		m_rb = m_obj.GetComponent<Rigidbody> ();
		//m_sprite = m_obj.transform.Find ("sprite").gameObject;
		//m_animation = m_sprite.GetComponent<Animation> ();
		//m_animator = m_sprite.GetComponent<Animator> ();
		//m_animator.speed = 0.0f;
		//m_playerID = GameManager.GetFreeID();

		Messenger.AddListener< float >(EVENT.UPDATE, OnUpdate );
		Messenger.AddListener< int,Vector3 >(EVENT.PRESSED, OnPressed );
		Messenger.AddListener< int, Vector3, string >(EVENT.CLICKED, OnClicked );
	}
	
	public void OnDisable()
	{
		Messenger.RemoveListener< float >(EVENT.UPDATE, OnUpdate );
		Messenger.RemoveListener< int,Vector3 >(EVENT.PRESSED, OnPressed );
	}
	
	// Update is called once per frame
	void OnUpdate (float elapsedTime)
	{
		//if( m_photonView.isMine == true )
		{
			m_position = m_rb.position;
			float fdist = Vector3.Distance (m_position, m_togo);
			if (fdist > 0.309351f)
			{	m_dir = m_togo - m_position;
				m_dir.Normalize ();
				m_rb.velocity = m_dir * m_maxspeed * elapsedTime;
			}
			else
			{	m_rb.velocity = Vector3.zero;
			}
			Messenger.Broadcast< Vector3 > (EVENT.CAMERA_UPDATE_SHIFTED_TARGET, m_position);
		}
	}

	public void OnClicked(int butt, Vector3 pos, string name)
	{
		if (name.Contains (m_obj.name))
		{
			Debug.Log ("i was clicked");
		}
	}
	//----------------------------------------
	//debug
	//----------------------------------------
	void OnGUI()
	{
		/*
		float fdist = Vector3.Distance (m_position, m_togo);
		GUILayout.BeginArea(new Rect(0,90,180,30));
		GUILayout.Label("fdist="+fdist);
		GUILayout.EndArea();
		*/
	}

	//----------------------------------------
	// NETWORKING
	//----------------------------------------
	//----------------------------------------
	// NETWORKING
	//----------------------------------------
}
