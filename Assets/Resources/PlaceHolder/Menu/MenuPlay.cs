﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif //UNITY_EDITOR

//----------------------------------------
// NETWORKING
//----------------------------------------
//----------------------------------------
// fin NETWORKING
//----------------------------------------

public class MenuPlay : MonoBehaviour
{

	//----------------------------------------
	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
	}
	//----------------------------------------

	//----------------------------------------
	//----------------------------------------
	public void OnConnectDone()
	{
		Debug.Log ("P2P connected");
	}
	//----------------------------------------
	//----------------------------------------
	public void OnClickedPlayAlone()
	{
	}

	public void OnClickedMultiPlayerCreate()
	{
		//p2p.m_callback = OnConnectDone;
		//WarpClient.GetInstance().Connect(GameManager.m_username);
	}

	public void OnClickedMultiPlayerJoin()
	{
	}

	public void OnMenuBack()
	{
		Messenger.Broadcast(EVENT.MENU_BACK);
	}
	

	public void OnClickedQuit()
	{
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#endif // UNITY_EDITOR
		Application.Quit ();

	}
}
